/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beast.geolite

import com.esotericsoftware.kryo.io.{Input, Output}
import com.esotericsoftware.kryo.{Kryo, KryoSerializable}

import java.awt.geom.{AffineTransform, Point2D}

/**
 * A class that holds metadata of a raster layer
 * @param x1 The index of the lowest column in the raster file (inclusive)
 * @param y1 The index of the lowest row in the raster file (inclusive)
 * @param x2 The index of the highest column in the raster file (exclusive)
 * @param y2 The highest index of a row in the raster layer (exclusive)
 * @param tileWidth The width of each tile in pixels
 * @param tileHeight he height of each tile in pixels
 * @param srid The spatial reference identifier of the coordinate reference system of this raster layer
 * @param g2m the grid to model affine transformation
 */
class RasterMetadata(var x1: Int, var y1: Int, var x2: Int, var y2: Int,
                     var tileWidth: Int, var tileHeight: Int,
                     var srid: Int, val g2m: AffineTransform) extends Serializable with KryoSerializable {
  // Note: This class is supposed to be immutable. However, we use "var" instead of "val" to enable Kryo serialization
  def this() {
    // Default constructor for Kryo serialization
    this(0, 0, 0, 0, 0, 0, 0, new AffineTransform())
  }

  /**Total number of columns in the raster layer*/
  def rasterWidth: Int = x2 - x1

  /**Total number of rows (scanlines) in the raster layer*/
  def rasterHeight: Int = y2 - y1

  /**Number of tiles per row*/
  def numTilesX: Int = (rasterWidth + tileWidth - 1) / tileWidth

  /**Number of tiles per column*/
  def numTilesY: Int = (rasterHeight + tileHeight - 1) / tileHeight

  /**Total number of tiles in the raster layer*/
  def numTiles: Int = numTilesX * numTilesY

  /**
   * Computes the ID of the tile that contains the given pixel. Tiles are numbered in row-wise ordering.
   * @param iPixel the position of the column of the pixel
   * @param jPixel the position of the row of the pixel
   * @return a unique identifier for the tile that contains this pixel location
   */
  def getTileIDAtPixel(iPixel: Int, jPixel: Int): Int = {
    val iTile = (iPixel - x1) / tileWidth
    val jTile = (jPixel - y1) / tileHeight
    jTile * numTilesX + iTile
  }

  /**
   * Returns the ID of the tile that contains the given point location in model (world) space
   * @param x the x-coordinate of the point, e.g., longitude
   * @param y the y-coordinate of the point, e.g., latitude
   * @return the ID of the tile that contains this pixel or -1 if the point is outside the input space
   */
  def getTileIDAtPoint(x: Double, y: Double): Int = {
    val pixel: Point2D.Double = new Point2D.Double()
    modelToGrid(x, y, pixel)
    getTileIDAtPixel(pixel.x.toInt, pixel.y.toInt)
  }

  def getPixelScaleX: Double = {
    val pt = new Point2D.Double()
    gridToModel(0, 0, pt)
    val x1 = pt.x
    gridToModel(1, 0, pt)
    val x2 = pt.x
    (x2 - x1).abs
  }

  /**
   * Computes the low index of a column in the given tile (inclusive)
   * @param tileID the ID of the tile
   * @return
   */
  def getTileX1(tileID: Int): Int = {
    val iTile = tileID % numTilesX
    x1 + iTile * tileWidth
  }

  /**
   * Computes the high index of a column in the given tile (inclusive)
   * @param tileID the ID of the tile
   * @return
   */
  def getTileX2(tileID: Int): Int = {
    val iTile = tileID % numTilesX
    x1 + (iTile + 1) * tileWidth - 1
  }

  /**
   * Computes the low index of a row (scanline) in the given tile (inclusive)
   * @param tileID the ID of the tile
   * @return
   */
  def getTileY1(tileID: Int): Int = {
    val iTile = tileID / numTilesX
    y1 + iTile * tileHeight
  }

  /**
   * Computes the high index of a row (scanline) in the given tile (inclusive)
   * @param tileID the ID of the tile
   * @return
   */
  def getTileY2(tileID: Int): Int = {
    val iTile = tileID / numTilesX
    y1 + (iTile + 1) * tileHeight - 1
  }

  /**
   * Converts a point location from the grid (pixel) space to the model (world) space
   * @param i the position of the column
   * @param j the position of the row
   * @param outPoint the output point that contains the model coordinates
   */
  def gridToModel(i: Double, j: Double, outPoint: Point2D.Double): Unit = {
    outPoint.setLocation(i, j)
    g2m.transform(outPoint, outPoint)
  }

  /**
   * Converts a point location from model (world) space to grid (pixel) space
   * @param x the x-coordinate in the model space (e.g., longitude)
   * @param y the y-coordinate in the model space (e.g., latitude)
   * @param outPoint the output point that contains the grid coordinates
   */
  def modelToGrid(x: Double, y: Double, outPoint: Point2D.Double): Unit = {
    outPoint.setLocation(x, y)
    g2m.inverseTransform(outPoint, outPoint)
  }

  override def equals(obj: Any): Boolean = {
    if (!obj.isInstanceOf[RasterMetadata]) return false
    val that = obj.asInstanceOf[RasterMetadata]
    (this eq that) || (this.x1 == that.x1 && this.x2 == that.x2 && this.y1 == that.y1 && this.y2 == that.y2 &&
      this.tileWidth == that.tileWidth && this.tileHeight == that.tileHeight && this.srid == that.srid &&
      this.g2m.equals(that.g2m))
  }

  override def hashCode(): Int = {
    x1.hashCode() + x2.hashCode() + y1.hashCode() + y2.hashCode() + tileWidth.hashCode() + tileHeight.hashCode() +
      srid.hashCode() + g2m.hashCode()
  }

  override def toString: String = {
    s"RasterMetadata ($x1, $y1)-($x2, $y2) tile size ${tileWidth}X${tileHeight} SRID: $srid G2M: $g2m"
  }

  override def write(kryo: Kryo, output: Output): Unit = {
    output.writeInt(x1)
    output.writeInt(y1)
    output.writeInt(x2)
    output.writeInt(y2)
    output.writeInt(tileWidth)
    output.writeInt(tileHeight)
    output.writeInt(srid)
    val matrix = new Array[Double](6)
    g2m.getMatrix(matrix)
    output.writeDoubles(matrix)
  }

  override def read(kryo: Kryo, input: Input): Unit = {
    this.x1 = input.readInt()
    this.y1 = input.readInt()
    this.x2 = input.readInt()
    this.y2 = input.readInt()
    this.tileWidth = input.readInt()
    this.tileHeight = input.readInt()
    this.srid = input.readInt()
    val matrix = input.readDoubles(6)
    this.g2m.setTransform(matrix(0), matrix(1), matrix(2), matrix(3), matrix(4), matrix(5))
  }
}
