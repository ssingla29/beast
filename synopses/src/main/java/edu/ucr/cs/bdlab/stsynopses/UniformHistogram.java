/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.stsynopses;

import com.sun.org.apache.xpath.internal.operations.Bool;
import edu.ucr.cs.bdlab.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.geolite.PointND;
import edu.ucr.cs.bdlab.geolite.GeometryHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * A uniform grid histogram storing Long values.
 */
public class UniformHistogram extends AbstractHistogram {
    /**Logger for this class*/
    private static final Log LOG = LogFactory.getLog(UniformHistogram.class);

    /**Dimensions of the grid of the histogram*/
    protected int[] numPartitions;

    /**The frequency value of the histogram*/
    protected long[] values;

    /**Default constructor is needed for deserialization*/
    public UniformHistogram() { }

    public UniformHistogram(EnvelopeND mbr, int ... numPartitions) {
        this.set(mbr);
        this.numPartitions = Arrays.copyOf(numPartitions, numPartitions.length);
        int totalLength = 1;
        for (int $d = 0; $d < numPartitions.length; $d++)
            totalLength *= numPartitions[$d];
        values = new long[totalLength];
    }

    /**
     * Computes a reasonable number of partitions along each axis to produce at most (but not much lower) than the
     * given number of buckets in the grid. It tries to make the side lengths of each cell as equal as possible.
     * In other words, it tries to produce cells that are closes to squares (or cubes ...).
     * @param mbr
     * @param numBuckets
     * @return
     */
    public static int[] computeNumPartitions(EnvelopeND mbr, int numBuckets) {
        double totalVolume = mbr.getArea();
        double cellVolume = totalVolume / numBuckets;
        double cellSideLength = Math.pow(cellVolume, 1.0 / mbr.getCoordinateDimension());
        int[] numPartitions = new int[mbr.getCoordinateDimension()];
        long totalNumPartitions = 1;
        for (int $d = 0; $d < mbr.getCoordinateDimension(); $d++) {
            numPartitions[$d] = Math.max(1, (int) Math.round(mbr.getSideLength($d) / cellSideLength));
            if (totalNumPartitions * numPartitions[$d] > numBuckets)
                numPartitions[$d] = (int) Math.max(1, numBuckets / totalNumPartitions);
            totalNumPartitions *= numPartitions[$d];
        }
        return numPartitions;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        GeometryHelper.writeEnvelope(this, out);
        for (int d = 0; d < getCoordinateDimension(); d++)
            out.writeInt(numPartitions[d]);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream gzos = new GZIPOutputStream(baos);
        // Reserve a buffer for writing values efficiently
        ByteBuffer bbuffer = ByteBuffer.allocate(1024 * 8);
        for (int i$ = 0; i$ < values.length; i$++) {
            bbuffer.putLong(values[i$]);
            if (bbuffer.position() == bbuffer.capacity()) {
                // Write the current buffer
                gzos.write(bbuffer.array(), 0, bbuffer.position());
                bbuffer.position(0);
            }
        }
        // Write any remainder in the buffer
        gzos.write(bbuffer.array(), 0, bbuffer.position());
        gzos.close();

        byte[] serializedData = baos.toByteArray();
        out.writeInt(serializedData.length);
        out.write(serializedData);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        GeometryHelper.readEnvelope(in, this);
        this.numPartitions = new int[getCoordinateDimension()];
        int arrayLength = 1;
        for (int d = 0; d < getCoordinateDimension(); d++) {
            numPartitions[d] = in.readInt();
            arrayLength *= numPartitions[d];
        }
        if (values == null || arrayLength != values.length)
            values = new long[arrayLength];
        int compressedDataLength = in.readInt();
        byte[] compressedData = new byte[compressedDataLength];
        in.readFully(compressedData);
        ByteArrayInputStream bais = new ByteArrayInputStream(compressedData);
        GZIPInputStream gzis = new GZIPInputStream(bais);
        ByteBuffer bbuffer = ByteBuffer.allocate(1024 * 8);
        bbuffer.limit(0);
        for (int i = 0; i < arrayLength; i++) {
            while (bbuffer.remaining() < 8) {
                // Move any remaining bytes to the beginning of the buffer
                System.arraycopy(bbuffer.array(), bbuffer.position(), bbuffer.array(), 0, bbuffer.remaining());
                // Read as much as we can after that
                // bbuffer.remaining() represents the current size of the bbuffer (left over from last read)
                // bbuffer-capacity() - bbuffer.remaining is how many bytes we can still add to the bbuffer
                int maxReadSize = (int) Math.min(bbuffer.capacity() - bbuffer.remaining(), (arrayLength - i) * 8L);
                int readSize = gzis.read(bbuffer.array(), bbuffer.remaining(), maxReadSize);
                bbuffer.limit(readSize + bbuffer.remaining());
                bbuffer.position(0);
            }
            values[i] = bbuffer.getLong();
        }
    }

    /**
     * Adds a value to a specific entry position in the histogram
     * @param position
     * @param size
     */
    public void addEntry(int[] position, long size) {
        assert position.length == getCoordinateDimension();
        int d = position.length;
        int pos = 0;
        while (d-- > 0) {
            pos *= numPartitions[d];
            pos += position[d];
        }
        if (pos >= 0 && pos < values.length)
            values[pos] += size;
    }

    /**
     * Add a value in the entry at the given point location.
     * @param coord the coordinates of the point
     * @param size
     */
    public void addPoint(double[] coord, long size) {
        assert coord.length == getCoordinateDimension();
        int[] position = new int[coord.length];
        for (int d = 0; d < coord.length; d++) {
            position[d] = (int) Math.floor((coord[d] - this.getMinCoord(d)) * this.numPartitions[d] / this.getSideLength(d));
            position[d] = Math.min(position[d], numPartitions[d] - 1);
        }
        addEntry(position, size);
    }

    public void addPoint(PointND p, long size) {
        assert p.getCoordinateDimension() == getCoordinateDimension();
        int[] position = new int[p.getCoordinateDimension()];
        for (int d = 0; d < p.getCoordinateDimension(); d++) {
            position[d] = (int) Math.floor((p.getCoordinate(d) - this.getMinCoord(d)) * this.numPartitions[d] / this.getSideLength(d));
            position[d] = Math.min(position[d], numPartitions[d] - 1);
        }
        addEntry(position, size);
    }

    /**
     * Merges with another histogram that is perfectly aligned with this histogram, i.e., the same MBR and the same
     * number of rows and columns
     * @param another
     */
    public UniformHistogram mergeAligned(UniformHistogram another) {
        assert this.isAligned(another);
        for (int i = 0; i < values.length; i++)
            this.values[i] += another.values[i];
        return this;
    }

    public double getOverlap(UniformHistogram another, int[] p1, int[] p2, double[] start1, double[] start2, double[] end1, double[] end2)
    {
        double overlap = 1;

        for(int d = 0; d < this.getCoordinateDimension() ; d++){

            start1[d] = this.getMinCoord(d) + (p1[d] * ((this.getMaxCoord(d)-this.getMinCoord(d))/this.numPartitions[d]));
            end1[d] = this.getMinCoord(d) +  ((p1[d]+1) * ((this.getMaxCoord(d)-this.getMinCoord(d))/this.numPartitions[d]));
            start2[d] = another.getMinCoord(d) + (p2[d] * ((another.getMaxCoord(d)-another.getMinCoord(d))/another.numPartitions[d]));
            end2[d] = another.getMinCoord(d) + ( (p2[d]+1) * ((another.getMaxCoord(d)-another.getMinCoord(d))/another.numPartitions[d]));

            overlap *= (Math.min(end1[d],end2[d]) - Math.max(start1[d],start2[d]))/(end1[d]-start1[d]);
        }

        if(overlap < 0)
            overlap = 0;

        return overlap;
    }

    public int getBucketID(int[] i, UniformHistogram uniformHistogram)
    {
        int dims = uniformHistogram.getCoordinateDimension();
        int pos = 0;
        while (dims-- > 0) {
            pos *= uniformHistogram.getNumPartitions(dims);
            pos += i[dims];
        }

        return pos;
    }

    public UniformHistogram mergeNonAligned(UniformHistogram another){

        int numDimensions = this.getCoordinateDimension();
        int[] i = new int[numDimensions];
        int[] j = new int[numDimensions];
        double[] start1 = new double[numDimensions];
        double[] end1 = new double[numDimensions];
        double[] start2 = new double[numDimensions];
        double[] end2 = new double[numDimensions];
        for(int d = 0; d <this.getCoordinateDimension(); d++) {
            for (int dim = 0; dim < d; dim++) {
                i[dim] = 0;
                j[dim] = 0;
            }
            if (d != 0)
            {
                if (end2[d] < end1[d])
                    j[d]++;
                else if (end1[d] < end2[d])
                    i[d]++;
                else{
                    i[d]++;
                    j[d]++;
                }
            }



            while (i[d] < this.numPartitions[d] && j[d] < another.numPartitions[d]) {

                double overlap = getOverlap(another, i, j, start1, start2, end1, end2);
                another.values[getBucketID(j, another)] += (long) (this.values[getBucketID(i, this)] * overlap);

                for (int dim = 0; dim <=d; dim++)
                {
                    boolean flag = false;
                    if (end2[dim] < end1[dim])
                    {
                        j[dim]++;
                        flag = true;
                    }
                    else if (end1[dim] < end2[dim])
                    {
                        i[dim]++;
                        flag = true;
                    }
                    else {
                        i[dim]++;
                        j[dim]++;
                        flag = true;
                    }
                    if(dim < d && (i[dim]==this.getNumPartitions(dim) || j[dim]==another.getNumPartitions(dim)))
                    {
                        i[dim] = 0;
                        j[dim] = 0;
                        flag = false;
                    }

                    if(flag==true)
                        break;

                }
            }
        }
        return another;
    }

    /**
     * Tests if the histogram is perfectly aligned with another histogram, i.e., the same MBR and the same number of
     * rows and columns.
     * @param another
     * @return
     */
    public boolean isAligned(UniformHistogram another) {
        return super.equalsExact(another) && Arrays.equals(this.numPartitions, another.numPartitions);
    }

    /**
     * Returns the envelope of a cell given its column and row position. To avoid unnecessary object creation, the given
     * envelope is filled in with the coordinates of the given cell.
     * @param position
     * @param mbr the MBR to fill with the information. If {@code null}, a {@link NullPointerException} is thrown.
     */
    public void getCellEnvelope(int[] position, EnvelopeND mbr) {
        mbr.setCoordinateDimension(this.getCoordinateDimension());
        for (int d = 0; d < this.getCoordinateDimension(); d++) {
            mbr.setMinCoord(d, (this.getMinCoord(d) * (numPartitions[d] - position[d]) + this.getMaxCoord(d) * position[d]) / numPartitions[d]);
            mbr.setMaxCoord(d, (this.getMinCoord(d) * (numPartitions[d] - (position[d]+ 1)) + this.getMaxCoord(d) * (position[d] + 1)) / numPartitions[d]);
        }
    }

    /**
     * Computes the sum of all values in the given range of grid cells.
     * @param minPos the position of the lower corner in grid coordinates
     * @param sizes the size (number of cells) along each dimension
     * @return
     */
    @Override
    public long getValue(int[] minPos, int[] sizes) {
        assert minPos.length == getCoordinateDimension();
        assert sizes.length == getCoordinateDimension();
        int[] pos = new int[minPos.length];
        int totalNumberOfCells = 1;
        boolean copyMade = false;
        for (int d = 0; d < minPos.length; d++) {
            if (minPos[d] < 0 || minPos[d] + sizes[d] > numPartitions[d]) {
                // Adjust minPos and sizes to account for the negative value
                if (!copyMade) {
                    minPos = Arrays.copyOf(minPos, minPos.length);
                    sizes = Arrays.copyOf(sizes, sizes.length);
                    copyMade = true;
                }
                if (minPos[d] < 0) {
                    sizes[d] += minPos[d]; // Reduce the size along this dimension
                    minPos[d] = 0; // Reset the minimum position to zero along this dimension
                }
                if (minPos[d] + sizes[d] > numPartitions[d])
                    sizes[d] = numPartitions[d] - minPos[d];
                if (sizes[d] <= 0)
                    return 0;
            }
            totalNumberOfCells *= sizes[d];
        }
        long sum = 0;
        for (int i = 0; i < totalNumberOfCells; i++) {
            int d = minPos.length;
            int offset = 0;
            while (d-- > 0) {
                offset *= numPartitions[d];
                offset += minPos[d] + pos[d];
            }
            sum += values[offset];
            // Move to the next position
            d = 0;
            while (d < pos.length && ++pos[d] >= sizes[d])
                pos[d++] = 0;
        }
        return sum;
    }

    /**
     * Print the histogram information to a text output stream for debugging purposes.
     * @param out
     */
    public void print(PrintStream out) {
        out.println("Column\tRow\tGeometry\tFrequency");
        EnvelopeND env = new EnvelopeND();
        for (int row = 0; row < getNumPartitions(1); row++) {
            for (int col = 0; col < getNumPartitions(0); col++) {
                getCellEnvelope(new int[] {col, row}, env);
                out.printf("%d\t%d\t%s\t%d\n", col, row, env.toText(), getValue(new int[] {col, row}, new int[] {1, 1}));
            }
        }
    }

    /**
     * Computes the sum of all entries in a given rectangle. It is assumed that x1 &le; x2 and y1 &le; y2
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public long sumRectangle(double x1, double y1, double x2, double y2) {
        int col1 = (int) Math.floor((x1 - this.getMinCoord(0)) * numPartitions[0] / this.getSideLength(0));
        int col2 = (int) Math.ceil((x2 - this.getMinCoord(0)) * numPartitions[0] / this.getSideLength(0));
        int row1 = (int) Math.floor((y1 - this.getMinCoord(1)) * numPartitions[1] / this.getSideLength(1));
        int row2 = (int) Math.ceil((y2 - this.getMinCoord(1)) * numPartitions[1] / this.getSideLength(1));
        // Compute the fraction in each direction that is outside the expanded region. These are the parts that need
        // to be subtracted. The variable naming assumes the coordinates increase from left to right and from top to bottom
        double fractionLeft = (x1 - this.getMinCoord(0)) / (this.getSideLength(0) / this.numPartitions[0]);
        fractionLeft -= Math.floor(fractionLeft);
        double fractionTop = (y1 - this.getMinCoord(1)) / (this.getSideLength(1) / this.numPartitions[1]);
        fractionTop -= Math.floor(fractionTop);
        double fractionRight = (this.getMaxCoord(0) - x2) / (this.getSideLength(0) / this.numPartitions[0]);
        fractionRight -= Math.floor(fractionRight);
        double fractionBottom = (this.getMaxCoord(1) - y2) / (this.getSideLength(1) / this.numPartitions[1]);
        fractionBottom -= Math.floor(fractionBottom);

        double expandedSum = getValue(new int[] {col1, row1}, new int[] {col2 - col1, row2 - row1});
        // Subtract the left column
        expandedSum -= fractionLeft * getValue(new int[] {col1, row1}, new int[] {1, row2 - row1});
        // Subtract the right column
        expandedSum -= fractionRight * getValue(new int[] {col2-1, row1}, new int[] {1, row2 - row1});
        // Subtract the top row
        expandedSum -= fractionTop * getValue(new int[] {col1, row1}, new int[] {col2 - col1, 1});
        // Subtract the bottom row
        expandedSum -= fractionBottom * getValue(new int[] {col1, row2-1}, new int[] {col2 - col1, 1});

        // Add back the top-left fraction that was subtracted twice
        expandedSum += fractionTop * fractionLeft * getValue(new int[] {col1, row1}, new int[] {1, 1});
        // Add back the bottom-left fraction that was subtracted twice
        expandedSum += fractionBottom * fractionLeft * getValue(new int[] {col1, row2-1}, new int[] {1, 1});
        // Add back the top-right fraction that was subtracted twice
        expandedSum += fractionTop * fractionRight * getValue(new int[] {col2-1, row1}, new int[] {1, 1});
        // Add back the bottom-right fraction that was subtracted twice
        expandedSum += fractionBottom * fractionRight * getValue(new int[] {col2-1, row2-1}, new int[] {1, 1});
        return Math.round(expandedSum);
    }

    public int getNumPartitions(int d) {
        return numPartitions[d];
    }

    /**
     * Increment a bin given its position in the array of bins
     * @param binID the ID of the bin to increment. Must be in the range [0, {@link #getNumBins()}[.
     * @param count the increment amount
     * @see #getBinID(double[])
     */
    public void incrementBin(int binID, long count) {
        values[binID] += count;
    }

    @Override
    public int getNumBins() {
        return values.length;
    }

    @Override
    public long getBinValue(int binID) {
        return values[binID];
    }
}