package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.util.CompactLongArray
import org.apache.spark.test.ScalaSparkTest
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CompactIntersectionsIterator2Test extends FunSuite with ScalaSparkTest {

  test("Iterate over two intersections") {
    val compactIntersections1 = new CompactIntersections(
      new CompactLongArray(Array[Long](1, 1, 1, 1, 2, 2, 2, 4)), // tile IDs
      new CompactLongArray(Array[Long](2, 2, 3, 3, 2, 3, 4, 3)), // ys
      new CompactLongArray(Array[Long](1, 2, 4, 5, 1, 2, 4, 5, 1, 3, 1, 2, 1, 1, 3, 3)), // xs
      new CompactLongArray(Array[Long](1, 1, 2, 2, 1, 1, 2, 1)), // geometry indexes
      Array[Long](1, 2, 3)) // featureIDs

    val compactIntersections2 = new CompactIntersections(
      new CompactLongArray(Array[Long](1, 1, 2)), // tile IDs
      new CompactLongArray(Array[Long](1, 2, 1)), // ys
      new CompactLongArray(Array[Long](2, 3, 2, 3, 3, 4)), // xs
      new CompactLongArray(Array[Long](1, 2, 1)), // geometry indexes
      Array[Long](1, 2, 3)) // feature IDs

    val breaker = new CompactIntersectionsTileBreaker(Array((1, compactIntersections1), (2, compactIntersections2)).iterator)

    val pixelRanges = new CompactIntersectionsIterator2(breaker)
    assertResult(11)(pixelRanges.size)
  }
}
