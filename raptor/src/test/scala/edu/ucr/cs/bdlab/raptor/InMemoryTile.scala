/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import org.apache.spark.sql.types.{DataType, IntegerType}

/**
 * An in-memory tile
 */
class InMemoryTile(override val tileID: Int,
                   override val rasterMetadata: RasterMetadata,
                   values: Array[Int]) extends ITile {

  def offset(i: Int, j: Int): Int = (j - y1) * this.tileWidth + (i - x1)

  override def isEmpty(i: Int, j: Int): Boolean = values(offset(i, j)) == 0

  override def numComponents: Int = 1

  override def getPixelValue(i: Int, j: Int): Any = values(offset(i, j))

  override def getPixelType: DataType = IntegerType
  override def maximum: Float = ???
  override def minimum: Float = ???

}
