package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.RasterMetadata
import org.apache.spark.test.ScalaSparkTest
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import java.awt.geom.AffineTransform

@RunWith(classOf[JUnitRunner])
class ITileTest extends FunSuite with ScalaSparkTest {

  test("Data conversion in ITile") {
    val metadata = new RasterMetadata(0, 0, 100, 100, 100, 100, 4326,
      new AffineTransform())
    val pixelValues = new Array[Int](100 * 100)
    pixelValues(10) = 123
    val originalTile = new InMemoryTile(0, metadata, pixelValues)

    assertResult(123)(originalTile.getPixelValueAs[Int](10, 0))
    assertResult(123.0f)(originalTile.getPixelValueAs[Float](10, 0))
    assertResult(Array(123))(originalTile.getPixelValueAs[Array[Int]](10, 0))
    assertResult(Array(123.0f))(originalTile.getPixelValueAs[Array[Float]](10, 0))
  }
}
