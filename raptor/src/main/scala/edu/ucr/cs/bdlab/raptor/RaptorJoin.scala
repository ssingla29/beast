/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes.RasterRDD
import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{IFeature, ITile, RasterMetadata}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.spark.HashPartitioner
import org.apache.spark.api.java.{JavaPairRDD, JavaRDD}
import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator
import org.locationtech.jts.geom.Geometry

import java.util
import scala.reflect.ClassTag

object RaptorJoin {
  /**
   * Performs a raptor join (Raster+Vector) between a set of raster files and the given set
   * of vector features. Each feature is given a numeric unique ID that is used to produce the output.
   * The return value is a distributed set of results with the following information:
   *  - FeatureID: Long - The ID of the geometry
   *  - RasterID: Int - The index of the file in the given list of files
   *  - x: Int - The x-coordinate of the pixel in the raster file
   *  - y: Int - The y-coordinate of the pixel in the raster file
   *  - m: Float - The value associated with the pixel
   *
   * @param rasters an array of files to process. The index of each entry in this array will be used to associate
   *                the output results to files.
   * @param featureIDs an RDD of pairs of ID and Feature. The ID is used in the output as a reference to the feature.
   * @param opts additional options passed to the raster readers when created
   * @param numTiles an optional accumulator that counts the total number of tiles read
   * @return an RDD of tuples that contain (feature ID, raster ID, x, y, value)
   */
  def raptorJoinFile[T](rasters: Array[String], featureIDs: RDD[(Long, IFeature)], opts: BeastOptions,
                    numTiles: LongAccumulator = null)(implicit t: ClassTag[T]) : RDD[RaptorJoinResult[T]] = {
    // Create a Hadoop configuration but load it into BeastOptions because Hadoop Configuration is not serializable
    val hadoopConf = new BeastOptions(opts)
    val sparkConf = featureIDs.sparkContext.getConf
    val iEntry = featureIDs.sparkContext.hadoopConfiguration.iterator()
    while (iEntry.hasNext) {
      val entry = iEntry.next()
      hadoopConf.put(entry.getKey, entry.getValue)
    }
    // intersections represent the following information
    // (Tile ID, (Geometry ID, Y, X1, X2))
    val intersectionsRDD: RDD[(Long, PixelRange)] = featureIDs.mapPartitions { idFeatures: Iterator[(Long, IFeature)] => {
      val conf = hadoopConf.loadIntoHadoopConf(new Configuration(false))
      val intersectionsList = new scala.collection.mutable.ListBuffer[Intersections]()
      val rasterList = new scala.collection.mutable.ListBuffer[Int]()

      val ids = new util.ArrayList[java.lang.Long]()
      val geoms: Array[Geometry] = idFeatures.map(x => {
        ids.add(x._1)
        x._2.getGeometry
      }).toArray

      for (i <- rasters.indices) {
        val raster = rasters(i)
        val rasterPath = new Path(raster)
        val filesystem = rasterPath.getFileSystem(conf)
        val rasterReader: IRasterReader = RasterHelper.createRasterReader(filesystem, rasterPath, opts, sparkConf)
        try {
          val intersections = new Intersections
          intersections.compute(ids, geoms, rasterReader.metadata, opts)
          if (intersections.getNumIntersections != 0) {
            intersectionsList.append(intersections)
            rasterList.append(i)
          }
        } finally {
          rasterReader.close()
        }
      }
      if (intersectionsList.isEmpty)
        Seq[(Long, PixelRange)]().iterator
      else
        new IntersectionsIterator(rasterList.toArray, intersectionsList.toArray)
    }}
    // Same format (Tile ID, (Geometry ID, Y, X1, X2))
    val orderedIntersections: RDD[(Long, PixelRange)] =
      intersectionsRDD.repartitionAndSortWithinPartitions(new HashPartitioner(intersectionsRDD.getNumPartitions))

    // Result is in the format (GeometryID, (RasterFileID, X, Y, RasterValue))
    orderedIntersections.mapPartitions(x =>
      new PixelIterator[T](x, rasters, opts.getString(IRasterReader.RasterLayerID), numTiles))
  }

  /**
   * Java shortcut to Raptor join between a set of raster files and a set of features each with a unique ID.
   * @param rasters a set of raster files names
   * @param featureIDs a set of (ID, Feature) pairs
   * @param opts additional operation for the query processor
   * @tparam T the data type of the pixels in the raster file
   * @return the set of Raptor join results that represents the matching pixels for each feature
   */
  def raptorJoinFileJ[T](rasters: Array[String], featureIDs: JavaPairRDD[java.lang.Long, IFeature], opts: BeastOptions):
      RDD[RaptorJoinResult[T]] = {
    implicit val ctagK: ClassTag[T] = ClassTag.AnyRef.asInstanceOf[ClassTag[T]]
    JavaRDD.fromRDD(raptorJoinFile[T](rasters, featureIDs.rdd.map(x => (x._1.longValue(), x._2)), opts))
  }

  /**
   * Raptor join operation between a list of raster file names and a set of features.
   * This method will first generate a unique ID for each feature, call the method [[raptorJoinFile()]]
   * and then join the result back with the features to include the entire feature in the result.
   * Therefore, it could be slower than [[[raptorJoinFile()]], especially when operating on big data.
   * @param rasters the list of files names for raster files
   * @param features the set of features
   * @param opts additional options for the query processor
   * @param numTiles an optional accumulator to collect number of tiles processed in the operation
   * @tparam T the type of the results which should match the data type of pixels
   * @return the resulting RDD which contains all overlaps between features and pixels.
   */
  def raptorJoinFileFeature[T](rasters: Array[String], features: RDD[IFeature], opts: BeastOptions = new BeastOptions(),
                    numTiles: LongAccumulator)(implicit t: ClassTag[T]) : RDD[RaptorJoinFeature[T]] = {
    val featureIDs: RDD[(Long, IFeature)] = features.zipWithUniqueId().map(x => (x._2, x._1))
    val rjResult: RDD[RaptorJoinResult[T]] = raptorJoinFile(rasters, featureIDs, opts, numTiles)
    rjResult.map(x => (x.featureID, x))
      .join(featureIDs)
      .map(x => RaptorJoinFeature(x._2._2, x._2._1.rasterMetadata, x._2._1.x, x._2._1.y, x._2._1.m))
  }

  /**
   * Java shortcut for the Raptor join operation between a set of raster file names and a set of features
   * @param rasters the list of files names for raster files
   * @param features the set of features
   * @param opts additional options for the query processor
   * @tparam T the type of the results which should match the data type of pixels
   * @return the resulting RDD which contains all overlaps between features and pixels.
   */
  def raptorJoinFileFeatureJ[T](rasters: Array[String], features: JavaRDD[IFeature], opts: BeastOptions = new BeastOptions()):
      JavaRDD[RaptorJoinFeature[T]] = {
    implicit val ctagK: ClassTag[T] = ClassTag.AnyRef.asInstanceOf[ClassTag[T]]
    JavaRDD.fromRDD(raptorJoinFileFeature[T](rasters, features.rdd, opts, null))
  }

  /**
   * Performs a join between the given raster and vector data. The vector data contains a unique ID for each feature
   * which is returned by this function.
   * The return value is a distributed set that contains pairs of feature ID and raster measure value for each
   * pixel that matches a feature.
   * @param raster a set of tiles in an RDD[ITile]
   * @param featureIDs a set of (ID, feature) pairs.
   * @param opts additional options for the algorithm
   * @param numTiles an optional accumulator that counts the total number of tiles read
   * @return a set of (Feature ID, pixel) pairs in the form of (FeatureID, RasterFileID, x, y, m)
   */
  def raptorJoinIDM[T](raster: RasterRDD, featureIDs: RDD[(Long, IFeature)], opts: BeastOptions = new BeastOptions(),
                     numTiles: LongAccumulator = null)(implicit t: ClassTag[T]) : RDD[(Long, T)] =
    raptorJoinIDFull[T](raster, featureIDs, opts, numTiles)
      .map(x => (x.featureID, x.m))

  /**
   * A shortcut for Raptor join of raster and vector RDDs to Java.
   * @param raster the raster RDD
   * @param vector a vector RDD that contains pairs of unique feature ID and the feature.
   * @param opts additional options to pass to the query processes
   * @tparam T the type of the return value which is the same as the type of the pixel value
   * @return an RDD of type [[RaptorJoinResult]] which contains all pixels that match with the given features.
   */
  def raptorJoinIDMJ[T](raster: JavaRDD[ITile], vector: JavaPairRDD[java.lang.Long, IFeature], opts: BeastOptions)
      : JavaPairRDD[java.lang.Long, T] = {
    implicit val ctagK: ClassTag[T] = ClassTag.AnyRef.asInstanceOf[ClassTag[T]]
    JavaPairRDD.fromRDD(raptorJoinIDM[T](raster.rdd, vector.rdd.map(p => (p._1.longValue(), p._2)), opts)
      .map(p => (java.lang.Long.valueOf(p._1), p._2)))
  }

  /**
   * Performs a raptor join between a raster RDD and a set of features. The output contains information about all
   * pixels that match with the set of features.
   * @param raster the raster RDD that contains all the tiles to test
   * @param features the set of features to join with the raster data
   * @param opts additional options for the query processor
   * @param numTiles an optional accumulator to count the number of tiles accesses during the query processing.
   * @tparam T the type of the pixel values
   * @return the set of overlaps between pixels and features
   */
  def raptorJoinFeature[T](raster: RasterRDD, features: RDD[IFeature], opts: BeastOptions = new BeastOptions(),
                           numTiles: LongAccumulator = null)(implicit t: ClassTag[T]): RDD[RaptorJoinFeature[T]] = {
    val featureIDs: RDD[(Long, IFeature)] = features.zipWithUniqueId().map(x => (x._2, x._1))
    raptorJoinIDFull[T](raster, featureIDs, opts, numTiles)
      .map(x => (x.featureID, x))
      .join(featureIDs)
      .map(x => RaptorJoinFeature(x._2._2, x._2._1.rasterMetadata, x._2._1.x, x._2._1.y, x._2._1.m))
  }

  /**
   * Java shortcut.
   * Performs a raptor join between a raster RDD and a set of features. The output contains information about all
   * pixels that match with the set of features.
   * @param raster the raster RDD that contains all the tiles to test
   * @param features the set of features to join with the raster data
   * @param opts additional options for the query processor
   * @tparam T the type of the pixel values
   * @return the set of overlaps between pixels and features
   */
  def raptorJoinFeatureJ[T](raster: JavaRDD[ITile], features: JavaRDD[IFeature], opts: BeastOptions):
      JavaRDD[RaptorJoinFeature[T]] = {
    implicit val ctagK: ClassTag[T] = ClassTag.AnyRef.asInstanceOf[ClassTag[T]]
    JavaRDD.fromRDD(raptorJoinFeature[T](raster, features.rdd, opts, null))
  }

  def raptorJoinFeatureM[T](raster: RasterRDD, features: RDD[IFeature], opts: BeastOptions = new BeastOptions(),
                            numTiles: LongAccumulator = null)(implicit t: ClassTag[T]) : RDD[(IFeature, T)] = {
    val featureIDs: RDD[(Long, IFeature)] = features.zipWithUniqueId().map(x => (x._2, x._1))
    val rjResult: RDD[(Long, T)] = raptorJoinIDM[T](raster, featureIDs, opts, numTiles)
    rjResult.join(featureIDs)
      .map(x => (x._2._2, x._2._1))
  }

  /**
   * A Raptor join implementation that returns all the matches between features and pixels along with the raster
   * metadata that puts the pixel in context.
   * @param raster the RDD that contains the raster tiles
   * @param vector the RDD that contains the vector features and their unique IDs
   * @param opts additional options for the query processor
   * @tparam T the type of the pixel values
   * @return RDD that contains all overlaps between pixels and geometries
   */
  def raptorJoinIDFull[T](raster: RDD[ITile], vector: RDD[(Long, IFeature)], opts: BeastOptions,
                          numTiles: LongAccumulator = null, numRanges: LongAccumulator = null)
                         (implicit t: ClassTag[T]) : RDD[RaptorJoinResult[T]] = {
    require(raster.isInstanceOf[RasterFileRDD], "RaptorJoin currently only supports RasterFileRDD")
    import edu.ucr.cs.bdlab.raptor.RaptorMixin._
    val allMetadata: Array[RasterMetadata] = raster.allMetadata
    val intersectionsRDD: RDD[(Long, CompactIntersections)] = vector.mapPartitions { idFeatures: Iterator[(Long, IFeature)] => {
      val ids = new util.ArrayList[java.lang.Long]()
      val geoms: Array[Geometry] = idFeatures.map(x => {
        ids.add(x._1)
        x._2.getGeometry
      }).toArray

      new CompactIntersectionsTileBreaker(allMetadata.indices.map(i => {
        val rasterMetadata = allMetadata(i)
        val intersections = new Intersections
          intersections.compute(ids, geoms, rasterMetadata, opts)
          //catch {case e: IllegalArgumentException  => throw new IllegalArgumentException(rasterMetadata + "," + i)}

        if (intersections.getNumIntersections == 0)
          (-1, null)
        else
          (i, new CompactIntersections(intersections))
      }).filterNot(_._1 == -1).iterator)
    }}

    val partitioner = new HashPartitioner(intersectionsRDD.getNumPartitions max raster.getNumPartitions)
    // Same format (Tile ID, (Geometry ID, Y, X1, X2))
    val orderedIntersections: RDD[(Long, CompactIntersections)] =
      intersectionsRDD.repartitionAndSortWithinPartitions(partitioner)

    // Partition tiles to match the intersections
    val metadataToID: Map[RasterMetadata, Int] = allMetadata.zipWithIndex.toMap
    //val rastermetadata = raster.first().rasterMetadata
    val orderedTiles: RDD[(Long, ITile)] = raster.map(tile => {
      assert(metadataToID.contains(tile.rasterMetadata), s"Unexpected! Metadata ${tile.rasterMetadata} "
        /*  + s"not found in ${metadataToID.keys.mkString("\n")}"*/)
      val rasterID = metadataToID(tile.rasterMetadata)
      val rasterTileID = (rasterID.toLong << 32) | tile.tileID
      (rasterTileID, tile)
    }).repartitionAndSortWithinPartitions(partitioner)

    orderedIntersections.zipPartitions(orderedTiles, true)( (intersections, tiles) =>
      new PixelIterator3[T](new CompactIntersectionsIterator2(intersections), tiles, numTiles, numRanges))
  }


  def raptorJoinIDFullJ[T](raster: JavaRDD[ITile], vector: JavaPairRDD[java.lang.Long, IFeature], opts: BeastOptions):
    JavaRDD[RaptorJoinResult[T]] = {
    implicit val ctagK: ClassTag[T] = ClassTag.AnyRef.asInstanceOf[ClassTag[T]]
    JavaRDD.fromRDD(raptorJoinIDFull[T](raster.rdd, vector.rdd.map(x => (x._1.longValue(), x._2)), opts, null))
  }

}
