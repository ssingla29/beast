/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes.{RasterRDD, SpatialRDD}
import edu.ucr.cs.bdlab.beast.common.BeastOptions
import edu.ucr.cs.bdlab.beast.geolite.{IFeature, RasterMetadata}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import scala.reflect.ClassTag

trait RaptorMixin {

  implicit class RasterReadMixinFunctions(sc: SparkContext) {
    /**
     * Loads a GeoTIFF file as an RDD of tiles
     * @param path the path of the file
     * @param iLayer the index of the band to load (0 by default)
     * @param opts additional options for loading the file
     * @return a [[RasterRDD]] that represents all tiles in the file
     */
    def geoTiff(path: String, iLayer: Int = 0, opts: BeastOptions = new BeastOptions): RasterRDD = {
      if (!opts.contains(IRasterReader.RasterLayerID))
        opts.set(IRasterReader.RasterLayerID, iLayer)
      new RasterFileRDD(sc, path, opts)
    }

    def hdfFile(path: String, layer: String, opts: BeastOptions = new BeastOptions()): RasterRDD = {
      opts.set(IRasterReader.RasterLayerID, layer)
      new RasterFileRDD(sc, path, opts)
    }
  }

  implicit class RaptorMixinOperations2(featuresIds: RDD[(Long, IFeature)]) {
    def raptorJoin[T](rasters: Array[String], opts: BeastOptions = new BeastOptions)(implicit t: ClassTag[T]):
      RDD[RaptorJoinResult[T]] = RaptorJoin.raptorJoinFile[T](rasters, featuresIds, opts)
  }

  implicit class RaptorMixinOperations3(raster: RasterRDD) {
    /**
     * Returns the spatial reference ID (SRID) of the raster data.
     * This function returns the SRID of the first tile assuming that all tiles have the same SRID.
     * @return the SRID of the raster data or 0 if SRID is unknown or undefined
     */
    def getSRID: Int = raster.first().rasterMetadata.srid

    /**
     * Performs a Raptor join operation with a set of vector features.
     * @param features the set of features to join with
     * @param opts additional options to configure the operation
     * @tparam T the type of the value in the result. Should be compatible with the pixel type of the raster
     * @return all overlaps between the given features and the pixels.
     */
    def raptorJoin[T](features: SpatialRDD, opts: BeastOptions = new BeastOptions)(implicit t: ClassTag[T]):
      RDD[RaptorJoinFeature[T]] =
      RaptorJoin.raptorJoinFeature[T](raster, features, opts, null)

    /**
     * Save this RasterRDD as a set of geoTIFF files.
     * @param path the path to write the output geotiff files to
     * @param opts additional options for saving the GeoTIFF file
     */
    def saveAsGeoTiff(path: String, opts: BeastOptions = new BeastOptions): Unit =
      GeoTiffWriter.saveAsGeoTiff(raster, path, opts)

    /**
     * Returns an array of all unique metadata in this RDD
     * @return
     */
    def allMetadata: Array[RasterMetadata] = {
      // For efficiency, we assume that all metadata in each partition is the same
      raster.mapPartitions(tiles => {
        if (tiles.hasNext) {
          Some(tiles.next().rasterMetadata).iterator
        } else {
          null
        }
      }).distinct()
        .collect()
    }
  }

  implicit class RaptorMixinOperations(features: SpatialRDD) {
    /**
     * Performs a Raptor join with the given array of raster file.
     * @param rasters a list of raster files to join with
     * @return an RDD with entries in the following format:
     *         - IFeature: One of the input features
     *         - Float: The measure value from the raster file converted to Float
     */
    def raptorJoin[T](rasters: Array[String], opts: BeastOptions = new BeastOptions)(implicit t: ClassTag[T]):
      RDD[RaptorJoinFeature[T]] =
      RaptorJoin.raptorJoinFileFeature[T](rasters, features, opts, null)
  }
}

object RaptorMixin extends RaptorMixin
