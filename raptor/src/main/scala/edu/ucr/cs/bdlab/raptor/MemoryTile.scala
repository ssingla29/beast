package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.io.tiff.TiffConstants
import edu.ucr.cs.bdlab.beast.util.BitArray
import org.apache.spark.sql.types.DataType

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, DataInputStream, DataOutputStream}
import java.util.zip.{GZIPInputStream, GZIPOutputStream}
import scala.reflect.ClassTag

//ToDo: replace bitspersample with min,max function for the raster - defined based on bitspersample or hdf range
//function to set and get a value
///*with KryoSerializable with Externalizable*/
//BitArray, ,implicit t:ClassTag[T],
class MemoryTile[T : ClassTag](val tileID: Int,
                               val rasterMetadata: RasterMetadata,
                               val numComponents: Int,
                               var values: Array[T],
                               val minimum: Float,
                               val maximum: Float,
                               var bitmask: BitArray /*var bitmask: Array[Boolean]*/) extends ITile  {

  var compressionScheme: Int = TiffConstants.COMPRESSION_NONE
  var tileData: Array[Byte] = null

  def compressTileData(values: Array[T]) : Array[Byte] = {
    compressionScheme = TiffConstants.COMPRESSION_LZW
    val baos = new ByteArrayOutputStream
    val gos = new GZIPOutputStream(baos)
    val dos = new DataOutputStream(gos)
    for (value <- values) {
      dos.writeInt(value.asInstanceOf[Int])
    }
    dos.close()

    //gos.write(values.asInstanceOf[Array[Byte]])
//    for(i <- 0 until values.length)
//      gos.write(i)
   // gos.close()
    baos.toByteArray
  }

  /**
   * Return the decompressed array of bytes with the tile data. If the tile is not loaded, it lazily loads it.
   * The loaded data is not serialized to keep the serialization cost low.
   * @return an array of decompressed tile data
   */
  def getTileData(): Unit = {

    compressionScheme = TiffConstants.COMPRESSION_NONE
    val gin = new GZIPInputStream(new ByteArrayInputStream(tileData))
//    val values_temp = new Array[Byte](rasterMetadata.tileWidth * rasterMetadata.tileHeight)
//    var length: Int = 0
//    while (length < values_temp.length) {
//      length  += gin.read(values_temp,length,values_temp.length - length)
//    }
//    values = new Array[T](rasterMetadata.tileWidth * rasterMetadata.tileHeight)
//    values = values_temp.asInstanceOf[Array[T]]
    val din = new DataInputStream(gin)
    values = new Array[T](rasterMetadata.tileWidth * rasterMetadata.tileHeight)
    for (i <- 0 until values.length) {
      values(i) = din.readInt().asInstanceOf[T]
    }
    din.close()
    //gin.close()
    tileData = null
  }

  /**
   * The value of the given pixel which depends on the tile data type and number of bands.
   * For example, if the data type is integer with three bands, it will return an integer array of 3.
   * If the data type is float and one band, it will return a single float value.
   *
   * @param i the index of the column
   * @param j the index of the row
   * @return the pixel value
   */
  override def getPixelValue(i: Int, j: Int): T = {
    if(compressionScheme != TiffConstants.COMPRESSION_NONE)
      getTileData()
    values(((i-rasterMetadata.getTileX1(tileID))*rasterMetadata.tileHeight)+(j-rasterMetadata.getTileY1(tileID)))
  }

  /**
   * Check whether the given pixel is empty or not. A pixel is empty if it has no data or if its pixel value is
   * equal to a fill value defined by the raster layer.
   *
   * @param i the index of the column
   * @param j the index of the row
   * @return `true` if pixel has a valid value or `false` if it does not.
   */
  override def isEmpty(i: Int, j: Int): Boolean = {
    bitmask.get(((i-rasterMetadata.getTileX1(tileID))*rasterMetadata.tileHeight)+(j-rasterMetadata.getTileY1(tileID)))
    //bitmask(((i-rasterMetadata.getTileX1(tileID))*rasterMetadata.tileHeight)+(j-rasterMetadata.getTileY1(tileID)))
  }

  protected def getPixelOffset(i: Int, j: Int) =
    ((i - rasterMetadata.getTileX1(tileID))) * rasterMetadata.tileHeight + (j - rasterMetadata.getTileY1(tileID))

  def setPixel(i: Int, j: Int, value: T): Unit = {
    val offset = getPixelOffset(i, j)
    bitmask.set(offset, false)
    // bitmask(offset) = false
    values(offset) = value
  }

  def compress(): Unit =
  {
    tileData = compressTileData(values)
    values = null
  }

  /**
   * Returns the type of the pixel as an SQL data type
   *
   * @return the type of the pixel values
   */
  override def getPixelType: DataType = ???
}

