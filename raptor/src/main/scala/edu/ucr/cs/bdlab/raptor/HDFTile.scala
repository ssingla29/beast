/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import org.apache.spark.sql.types.{DataType, FloatType}

import java.awt.geom.Point2D
import java.nio.ByteBuffer

class HDFTile(override val tileID: Int, dataBuffer: ByteBuffer, valueSize: Int,
              fillValue: Int, scaleFactor: Double, override val rasterMetadata: RasterMetadata) extends ITile {

  lazy val resolution: Int = tileWidth

  override def getPixelValue(i: Int, j: Int): Any = {
    val intVal = valueSize match {
      case 1 => dataBuffer.get((j * resolution + i) * valueSize)
      case 2 => dataBuffer.getShort((j * resolution + i) * valueSize)
      case 4 => dataBuffer.getInt((j * resolution + i) * valueSize)
    }
    (intVal * scaleFactor).toFloat
  }

  override def isEmpty(i: Int, j: Int): Boolean = {
    valueSize match {
      case 1 =>
        dataBuffer.get((j * resolution + i) * valueSize) == fillValue
      case 2 =>
        dataBuffer.getShort((j * resolution + i) * valueSize) == fillValue
      case 4 =>
        dataBuffer.getInt((j * resolution + i) * valueSize) == fillValue
    }
  }

  override def numComponents: Int = 1

  override def getPixelType: DataType = FloatType

  /**   Minimum value that can be assigned to a pixel   */
  override def minimum: Float = ???

  /** Maximum value that can be assigned to a pixel */
  override def maximum: Float = ???
}
