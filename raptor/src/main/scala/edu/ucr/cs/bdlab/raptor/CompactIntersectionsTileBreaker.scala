package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.util.CompactLongArray

/**
 * Breaks CompactIntersections by tile. This class iterates over pairs of FileTileID and CompactIntersections.
 * FileTileID is a 64-bit long that is a concatenation of file ID (most significant) and tile ID (least significant)
 *  with 32-bit each.
 *
 * @param intersections the list of all intersections as pairs of file ID and intersections
 */
class CompactIntersectionsTileBreaker(intersections: Iterator[(Int, CompactIntersections)])
  extends Iterator[(Long, CompactIntersections)] {

  /**The tileID that will be returned on the next call*/
  var tileID: Int = _

  /**An iterator to tile IDs in the current intersections*/
  var tileIDs: CompactLongArray.CLLIterator = _

  /**An iterator to ys in the current intersections*/
  var ys: CompactLongArray.CLLIterator = _

  /**An iterator to ys in the current intersections*/
  var xs: CompactLongArray.CLLIterator = _

  /**An iterator to geometry indexes in the current intersections*/
  var geometryIndexes: CompactLongArray.CLLIterator = _

  /**The file ID currently being processed and will be returned by next() */
  var fileID: Long = _

  /**The compact Intersections currently being processed and from which next() will return its value*/
  var compactIntersections: CompactIntersections = _

  def moveToNextIntersections: Unit = {
    // Jump to the next file with non-empty intersections
    while (intersections.hasNext && (compactIntersections == null || compactIntersections.getNumIntersections == 0)) {
      val n = intersections.next()
      fileID = n._1
      compactIntersections = n._2
    }
    if (compactIntersections != null && compactIntersections.getNumIntersections > 0) {
      tileIDs = compactIntersections.tileID.iterator()
      ys = compactIntersections.ys.iterator()
      xs = compactIntersections.xs.iterator()
      geometryIndexes = compactIntersections.geometryIndexes.iterator()
      tileID = tileIDs.next().toInt
    }
  }

  // Initialize by moving to the next intersections
  moveToNextIntersections

  override def hasNext: Boolean = compactIntersections != null

  override def next(): (Long, CompactIntersections) = {
    var numIntersectionRanges: Int = 1
    var nextTileID: Int = -1
    if (tileIDs.hasNext) {
      do {
        nextTileID = tileIDs.next().toInt
        if (nextTileID == tileID)
          numIntersectionRanges += 1
      } while (tileIDs.hasNext && nextTileID == tileID)
    } else {
      // A special case when the last tile contains only one intersection
      nextTileID = tileID
    }
    val tileIDsNext: CompactLongArray = CompactLongArray.fill(tileID, numIntersectionRanges)
    val ysNext: CompactLongArray = ys.next(numIntersectionRanges)
    val xsNext: CompactLongArray = xs.next(2 * numIntersectionRanges)
    val geometryIndexesNext: CompactLongArray = geometryIndexes.next(numIntersectionRanges)
    val nextIntersections: CompactIntersections = new CompactIntersections(tileIDsNext, ysNext, xsNext,
      geometryIndexesNext, compactIntersections.featureIDs)
    val fileTileID: Long = (fileID << 32) | tileID
    // Move to the next intersections
    if (tileIDs.hasNext || tileID != nextTileID) {
      // More intersections still need to be processed in the current one
      tileID = nextTileID
    } else {
      compactIntersections = null
      // Move to the next one
      moveToNextIntersections
    }

    (fileTileID, nextIntersections)
  }
}
