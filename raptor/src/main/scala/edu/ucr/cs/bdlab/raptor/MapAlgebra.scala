package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.common.{BeastOptions, CLIOperation}
import edu.ucr.cs.bdlab.beast.geolite.ITile
import edu.ucr.cs.bdlab.beast.util.OperationMetadata
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.spark.SparkContext
import org.apache.spark.internal.Logging
import org.apache.spark.rdd.RDD
import org.geotools.referencing.CRS

@OperationMetadata(
  shortName = "mapalgebra",
  description = "Computes mapalgebra",
  inputArity = "6",
  outputArity = "1")
object MapAlgebra extends CLIOperation with Logging{

  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  /**
   * Run the main function using the given user command-line options and spark context
   *
   * @param opts    user options for configuring the operation
   * @param inputs  inputs provided by the user
   * @param outputs outputs provided by the user
   * @param sc      the Spark context used to run the operation
   * @return an optional result of this operation
   */


  def run(opts: BeastOptions, inputs: Array[String], outputs: Array[String], sc: SparkContext): Any = {
    //opts.set(SPLIT_MAXSIZE, inputs(1).toInt)
   // sc.hadoopConfiguration.setInt("mapred.min.split.size", 33554432)
    //sc.hadoopConfiguration.setInt("mapred.max.split.size", 33554432)
    val t0 = System.nanoTime()
    val fileTileRDD = new RasterFileRDD(sc, inputs(0), opts)
    var RasterRDD = fileTileRDD.asInstanceOf[RDD[ITile]]
    if(RasterRDD.getNumPartitions < inputs(1).toInt)
      RasterRDD = RasterRDD.repartition(inputs(1).toInt)
    //val histogram = MapAlgebraFocal.histogram(RasterRDD)
    //histogram.saveAsTextFile(outputs(0))

    //GeoTiffWriter.saveAsGeoTiff(RasterRDD, outputs(0),  Seq(GeoTiffWriter.BitsPerSample -> "8", GeoTiffWriter.SampleFormats->"1", GeoTiffWriter.WriteMode->"compatibility"))

    //GeoTiffWriter.saveAsGeoTiff(RasterRDD, outputs(0), Seq(GeoTiffWriter.BitsPerSample -> "8,8,8,8", GeoTiffWriter.SampleFormats->"1,1,1,1",  GeoTiffWriter.WriteMode->"compatibility"))

    //val threshold = MapAlgebraFocal.maxThreshold(RasterRDD, 50)
    //GeoTiffWriter.saveAsGeoTiff(threshold, outputs(0),  Seq(GeoTiffWriter.BitsPerSample -> "8", GeoTiffWriter.SampleFormats->"1"))

   // val stats = MapAlgebraFocal.minimum(RasterRDD)//MapAlgebraFocal.stats(RasterRDD)
    //System.out.println(stats)
//    System.out.println(stats._1 + "," + stats._2 + "," + stats._3 + "," + stats._4)
//    val mean: Array[Float] = new Array[Float](stats._3.length)
//    for(i <- 0 until stats._3.length)
//      mean(i) = stats._3(i)/stats._4(i)
//    System.out.println(MapAlgebraFocal.std_dev(RasterRDD, mean ,stats._4))

//    val focal = MapAlgebraFocal.focal(RasterRDD, 3)
//    GeoTiffWriter.saveAsGeoTiff(focal, outputs(0),  Seq(GeoTiffWriter.BitsPerSample -> "32", GeoTiffWriter.SampleFormats->"3"))

   val result = MapAlgebraFocal.reprojection(RasterRDD, CRS.decode("EPSG:4269"), inputs(2).toInt,inputs(3).toInt, inputs(4).toInt, inputs(5).toInt)
    //System.out.println(result)
    //val outputFile = new File(outputs(0))
    GeoTiffWriter.saveAsGeoTiff(result, outputs(0), Seq(GeoTiffWriter.BitsPerSample -> 8, GeoTiffWriter.SampleFormats->1))
    val t1 = System.nanoTime()
    System.out.println("Total Time: " + (t1-t0)/1E9)
  }
}
