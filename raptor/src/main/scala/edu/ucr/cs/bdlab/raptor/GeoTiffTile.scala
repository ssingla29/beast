/*
 * Copyright 2021 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.io.tiff.AbstractTiffTile
import org.apache.spark.sql.types.{ArrayType, DataType, FloatType, IntegerType, StructField, StructType}

/**
 * A tile from a GeoTIFF file
 */
class GeoTiffTile(override val tileID: Int, tiffTile: AbstractTiffTile,
                  fillValue: Int, override val rasterMetadata: RasterMetadata) extends ITile {

  override def isEmpty(i: Int, j: Int): Boolean = {
    for (c <- 0 until numComponents) {
      if (tiffTile.getSampleValueAsInt(i, j, c) == fillValue)
        return true
    }
    false
  }

  override def numComponents: Int = tiffTile.getNumSamples

  /**All sample types are float*/
  private lazy val allFloat: Boolean = (0 until numComponents).forall(i => tiffTile.getSampleFormat(i) == 3)

  /**All sample types are int*/
  private lazy val allInt: Boolean = (0 until numComponents).forall(i => tiffTile.getSampleFormat(i) <= 2)

  override def getPixelValue(i: Int, j: Int): Any = {
    if (numComponents == 1) {
      // A single value
      tiffTile.getSampleFormat(0) match {
        case 1 => tiffTile.getSampleValueAsInt(i, j, 0)
        case 2 => tiffTile.getSampleValueAsInt(i, j, 0)
        case 3 => tiffTile.getSampleValueAsFloat(i, j, 0)
      }
    } else if (allFloat) {
      val values = new Array[Float](numComponents)
      for (b <- values.indices)
        values(b) = tiffTile.getSampleValueAsFloat(i, j, b)
      values
    } else if (allInt) {
      val values = new Array[Int](numComponents)
      for (b <- values.indices)
        values(b) = tiffTile.getSampleValueAsInt(i, j, b)
      values
    } else {
      // Array of value
      val values = new Array[AnyVal](numComponents)
      for (b <- values.indices) {
        values(b) = tiffTile.getSampleFormat(b) match {
          case 1 => tiffTile.getSampleValueAsInt(i, j, b)
          case 2 => tiffTile.getSampleValueAsInt(i, j, b)
          case 3 => tiffTile.getSampleValueAsFloat(i, j, b)
        }
      }
      values
    }
  }

  override def getPixelType: DataType = {
    // A single component, return its data type directly
    if (numComponents == 1 && tiffTile.getSampleFormat(0) <= 2)
      return IntegerType
    if (numComponents == 1 && tiffTile.getSampleFormat(0) == 3)
      return FloatType
    // Multiple components of the same type, return an array type
    if (allInt)
      return ArrayType.apply(IntegerType)
    if (allFloat)
      return ArrayType.apply(FloatType)
    // A mix of types, return a struct type
    val pixelTypes: Array[DataType] = new Array[DataType](numComponents)
    for (i <- 0 until numComponents) {
      pixelTypes(i) = tiffTile.getSampleFormat(i) match {
        case 1 => IntegerType
        case 2 => IntegerType
        case 3 => FloatType
      }
    }
    StructType.apply(pixelTypes.zipWithIndex.map(x => StructField(s"#${x._2}", x._1)))
  }
  /**   Minimum value that can be assigned to a pixel   */
  override def minimum: Float = {
    val value = tiffTile.getSampleFormat(0) match {
      case 1 => 0 //unsigned int
      case 2 => Int.MinValue  //-(1 < (bitspersample - 1))
      case 3 => Float.NegativeInfinity
    }
    value
  }

  /** Maximum value that can be assigned to a pixel */
  override def maximum: Float = {
    val value = tiffTile.getSampleFormat(0) match {
      case 1 => 0//1 < bitspersample - 1
      case 2 => Int.MaxValue // (1 < bitspersample - 1) - 1
      case 3 => Float.PositiveInfinity
    }
    value
  }
}
