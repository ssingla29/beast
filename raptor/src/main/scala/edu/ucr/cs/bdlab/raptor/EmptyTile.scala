package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import org.apache.spark.sql.types.{DataType, IntegerType}

/**
 * A tile that is completely empty with no underlying data. This is used as a placeholder whenever
 * a tile does not exist in a raster layer.
 */
class EmptyTile(override val tileID: Int, override val rasterMetadata: RasterMetadata,
                override val numComponents: Int) extends ITile {

  override def isEmpty(i: Int, j: Int): Boolean = true

  // All remaining function should never be called since all pixels are empty
  override def getPixelValue(i: Int, j: Int): Any = throw new NullPointerException(s"Pixel ($i, $j) is empty")

  override def getPixelType: DataType = IntegerType

  /**   Minimum value that can be assigned to a pixel   */
  override def minimum: Float = throw new NullPointerException(s"All pixels are empty")

  /** Maximum value that can be assigned to a pixel */
  override def maximum: Float = throw new NullPointerException(s"All pixels are empty")
}
