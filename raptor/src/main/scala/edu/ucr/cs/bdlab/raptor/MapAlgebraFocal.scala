package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.geolite.{ITile, RasterMetadata}
import edu.ucr.cs.bdlab.beast.util.BitArray
import org.apache.spark.SparkConf
import org.apache.spark.beast.CRSServer
import org.apache.spark.rdd.RDD
import org.geotools.geometry.DirectPosition2D
import org.geotools.referencing.CRS
import org.opengis.referencing.crs.CoordinateReferenceSystem
import org.opengis.referencing.operation.MathTransform

import java.awt.geom.{AffineTransform, Point2D}
import java.util
import scala.collection.mutable
import scala.reflect.ClassTag

//ToDo: Make sure pixel access and assignment is correct
//ToDo: Make functions independent of pixel type

object MapAlgebraFocal {

  def focal(raster:RDD[ITile], size:Int):RDD[ITile] = {
    val partialTiles = raster.flatMap(tile => {
      val rows = tile.tileHeight
      val columns = tile.tileWidth
      val numbands = tile.numComponents

      val memoryTiles = new scala.collection.mutable.ArrayBuffer[(Int, (Int,MemoryTile[Array[Float]]))]()
      val newTile = Array.ofDim[Float](rows * columns, size * size)
     // val bitmask = Array.ofDim[Boolean](rows * columns)
      //util.Arrays.fill(bitmask,false)
      val bitmask = new BitArray(rows * columns)
      bitmask.fill(false)
      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2) {
          util.Arrays.fill(newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)),Float.NaN)
          bitmask.set((((i - tile.x1) * tile.tileHeight) + (j - tile.y1)), bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i, j))
          if (!tile.isEmpty(i, j)) {
            val pixelValues: Float = tile.getPixelValueAs[Float](i, j)
            newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(4) = pixelValues
          }
          //var iter: Int = 1
          //ToDo:correct this
          for (k <- 1 to size/2) {

            if ((i - k) >= tile.x1 && (i - k) <= tile.x2 && (j - k) >= tile.y1 && (j - k) <= tile.y2) {
              bitmask.set((((i - tile.x1) * tile.tileHeight) + (j - tile.y1)),  bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i - k, j - k))
              if (!tile.isEmpty(i - k, j - k)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i - k, j - k)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(0) = pixelValues
              }
              // iter = iter + 1
            }
            if ((j - k) >= tile.y1 && (j - k) <= tile.y2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1), bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i, j - k))
              if (!tile.isEmpty(i, j - k)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i, j - k)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(1) = pixelValues
              }
              //  iter = iter + 1
            }

            if ((i + k) >= tile.x1 && (i + k) <= tile.x2 && (j - k) >= tile.y1 && (j - k) <= tile.y2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1), bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i + k, j - k))
              if (!tile.isEmpty(i + k, j - k)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i + k, j - k)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(2) = pixelValues
              }
              // iter = iter + 1
            }

            if ((i - k) >= tile.x1 && (i - k) <= tile.x2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1),  bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i - k, j))
              if (!tile.isEmpty(i - k, j)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i - k, j)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(3) = pixelValues
              }
              // iter = iter + 1
            }

            if ((i + k) >= tile.x1 && (i + k) <= tile.x2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1), bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i + k, j))
              if (!tile.isEmpty(i + k, j)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i + k, j)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(5) = pixelValues
              }
              //iter = iter + 1
            }
            if ((i - k) >= tile.x1 && (i - k) <= tile.x2 && (j + k) >= tile.y1 && (j + k) <= tile.y2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1), bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i - k, j + k))
              if (!tile.isEmpty(i - k, j + k)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i - k, j + k)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(6) = pixelValues
              }
              // iter = iter + 1
            }

            if ((j + k) >= tile.y1 && (j + k) <= tile.y2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1),bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i, j + k))
              if (!tile.isEmpty(i, j + k)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i, j + k)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(7) = pixelValues
              }
              //iter = iter + 1
            }

            if ((i + k) >= tile.x1 && (i + k) <= tile.x2 && (j + k) >= tile.y1 && (j + k) <= tile.y2) {
              bitmask.set(((i - tile.x1) * tile.tileHeight) + (j - tile.y1), bitmask.get(((i - tile.x1) * tile.tileHeight) + (j - tile.y1)) | tile.isEmpty(i + k, j + k))
              if (!tile.isEmpty(i + k, j + k)) {
                val pixelValues: Float = tile.getPixelValueAs[Float](i + k, j + k)
                newTile(((i - tile.x1) * tile.tileHeight) + (j - tile.y1))(8) = pixelValues
              }
              // iter = iter + 1
            }
          }
        }
      }

      memoryTiles+= ((tile.tileID,(5,new MemoryTile[Array[Float]](tile.tileID, tile.rasterMetadata, numbands, newTile, tile.minimum, tile.maximum, bitmask))))

      val tileleft : Int = if ((tile.tileID + 1) % tile.rasterMetadata.numTilesX == 1) {Int.MinValue} else tile.tileID - 1
      val tileright : Int= if ((tile.tileID + 1) % tile.rasterMetadata.numTilesX == 0) {Int.MinValue} else tile.tileID + 1
      val tileup : Int = if ((tile.tileID - tile.rasterMetadata.numTilesX) < 0 ) {Int.MinValue} else tile.tileID - tile.rasterMetadata.numTilesX
      val tilebelow : Int = if ((tile.tileID + tile.rasterMetadata.numTilesX) > (tile.rasterMetadata.numTiles - 1) ) {Int.MinValue} else tile.tileID + tile.rasterMetadata.numTilesX

      val tilediagul : Int = if (tileup == Int.MinValue) {Int.MinValue} else if ((tileup + 1) % tile.rasterMetadata.numTilesX == 1) {Int.MinValue} else tileup - 1
      val tilediagur : Int = if (tileup == Int.MinValue) {Int.MinValue} else if ((tileup + 1) % tile.rasterMetadata.numTilesX == 0) {Int.MinValue} else tileup + 1
      val tilediagbl : Int = if (tilebelow == Int.MinValue) {Int.MinValue} else if ( (tilebelow + 1) % tile.rasterMetadata.numTilesX == 1) {Int.MinValue} else tilebelow - 1
      val tilediagbr : Int = if (tilebelow == Int.MinValue) {Int.MinValue} else if ( (tilebelow + 1) % tile.rasterMetadata.numTilesX == 0) {Int.MinValue} else tilebelow + 1


      if(tileleft != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tileleft)
        val x2 = tile.rasterMetadata.getTileX2(tileleft)
        val y1 = tile.rasterMetadata.getTileY1(tileleft)
        val y2 = tile.rasterMetadata.getTileY2(tileleft)
        val newTileleft = Array.ofDim[Float](rows * columns, size * size)
        //val bitmaskleft = Array.ofDim[Boolean](rows * columns)
        //util.Arrays.fill(bitmaskleft,false)
        val bitmaskleft = new BitArray(rows * columns)
        bitmaskleft.fill(false)
        for(i <- x2 to x2) {
          for (j <- y1 to y2) {
            util.Arrays.fill(newTileleft(((i - x1) * tile.tileHeight) + (j - y1)), Float.NaN)
            var iter: Int = 2
            //ToDo:correct this
            for (k <- 1 to (size/2)){
              for(l <- -(size/2) to (size/2)){
                if ((i + k) >= tile.x1 && (i + k) <= tile.x2 && (j+l) >= tile.y1 && (j+l) <= tile.y2) {
                  bitmaskleft.set(((i - x1) * tile.tileHeight) + (j - y1), bitmaskleft.get(((i - x1) * tile.tileHeight) + (j - y1)) | tile.isEmpty(i + k, j + l))
                  if (!tile.isEmpty(i + k, j + l)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i + k, j + l)
                    newTileleft(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                }
                iter = iter + size
              }
            }
          }
        }
        memoryTiles+= ((tileleft,(6,new MemoryTile[Array[Float]](tileleft, tile.rasterMetadata, numbands, newTileleft, tile.minimum, tile.maximum, bitmaskleft))))
      }
      if(tileright != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tileright)
        val x2 = tile.rasterMetadata.getTileX2(tileright)
        val y1 = tile.rasterMetadata.getTileY1(tileright)
        val y2 = tile.rasterMetadata.getTileY2(tileright)
        val newTileright = Array.ofDim[Float](rows * columns, size * size)
        //val bitmaskright = Array.ofDim[Boolean](rows * columns)
        //util.Arrays.fill(bitmaskright,false)
        val bitmaskright = new BitArray(rows * columns)
        bitmaskright.fill(false)
        for(i <- x1 to x1){
          for (j <- y1 to y2) {
            util.Arrays.fill(newTileright(((i - x1) * tile.tileHeight) + (j - y1)), Float.NaN)
            var iter: Int = 0
            //ToDo:correct this
            for (k <- 1 to size / 2) {
              for(l <- -(size/2) to size/2){
                if ((i - k) >= tile.x1 && (i - k) <= tile.x2 && (j+l) >= tile.y1 && (j+l) <= tile.y2) {
                  bitmaskright.set(((i - x1) * tile.tileHeight) + (j - y1), bitmaskright.get(((i - x1) * tile.tileHeight) + (j - y1))| tile.isEmpty(i-k, j + l))
                  if (!tile.isEmpty(i - k, j + l)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i - k, j + l)
                    newTileright(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                }
                iter = iter + size
              }
            }
          }
        }
        memoryTiles+= ((tileright,(4,new MemoryTile[Array[Float]](tileright, tile.rasterMetadata, numbands, newTileright, tile.minimum, tile.maximum, bitmaskright))))
      }
      if(tileup != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tileup)
        val x2 = tile.rasterMetadata.getTileX2(tileup)
        val y1 = tile.rasterMetadata.getTileY1(tileup)
        val y2 = tile.rasterMetadata.getTileY2(tileup)
        val newTileup = Array.ofDim[Float](rows * columns, size * size)
        //val bitmaskup = Array.ofDim[Boolean](rows * columns)
        ///util.Arrays.fill(bitmaskup,false)
        val bitmaskup = new BitArray(rows * columns)
        bitmaskup.fill(false)
        for (i <- x1 to x2) {
          for(j <- y2 to y2){
            util.Arrays.fill(newTileup(((i - x1) * tile.tileHeight) + (j - y1)),Float.NaN)
            var iter: Int = 6
            //ToDo:correct this
            for (k <- 1 to size/2) {
              for (l <- -(size/2) to size/2){
                if ((i + l) >= tile.x1 && (i + l) <= tile.x2 && (j + k) >= tile.y1 && (j + k) <= tile.y2) {
                  bitmaskup.set(((i - x1) * tile.tileHeight) + (j - y1), bitmaskup.get(((i - x1) * tile.tileHeight) + (j - y1))| tile.isEmpty(i + l, j + k))
                  if (!tile.isEmpty(i + l, j + k)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i + l, j + k)
                    newTileup(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                }
                iter = iter + 1
              }
            }
          }
        }
        memoryTiles+= ((tileup,(8,new MemoryTile[Array[Float]](tileup, tile.rasterMetadata, numbands, newTileup, tile.minimum, tile.maximum, bitmaskup))))
      }
      if(tilebelow != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tilebelow)
        val x2 = tile.rasterMetadata.getTileX2(tilebelow)
        val y1 = tile.rasterMetadata.getTileY1(tilebelow)
        val y2 = tile.rasterMetadata.getTileY2(tilebelow)
        val newTilebelow = Array.ofDim[Float](rows * columns, size * size)
        // bitmaskbelow = Array.ofDim[Boolean](rows * columns)
        //util.Arrays.fill(bitmaskbelow,false)
        val bitmaskbelow = new BitArray(rows * columns)
        bitmaskbelow.fill(false)
        for (i <- x1 to x2) {
          for(j <- y1 to y1){
            util.Arrays.fill(newTilebelow(((i - tile.x1) * tile.tileHeight) + (0)),Float.NaN)
            var iter: Int = 0
            //ToDo:correct this
            for (k <- 1 to size/2) {
              for(l <- -(size/2) to size/2){
                if ((i + l) >= tile.x1 && (i + l) <= tile.x2 && (j - k) >= tile.y1 && (j - k) <= tile.y2) {
                  bitmaskbelow.set(((i - x1) * tile.tileHeight) + (j - y1), bitmaskbelow.get(((i - x1) * tile.tileHeight) + (j - y1)) | tile.isEmpty(i + l, j - k))
                  if (!tile.isEmpty(i + l, j - k)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i + l, j - k)
                    newTilebelow(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                }
                iter = iter + 1

              }
            }
          }
        }
        val targetTile = new MemoryTile[Array[Float]](tilebelow, tile.rasterMetadata, numbands, newTilebelow, tile.minimum, tile.maximum, bitmaskbelow)
        memoryTiles+= ((tilebelow, (2,targetTile)))
      }

      if(tilediagul != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tilediagul)
        val x2 = tile.rasterMetadata.getTileX2(tilediagul)
        val y1 = tile.rasterMetadata.getTileY1(tilediagul)
        val y2 = tile.rasterMetadata.getTileY2(tilediagul)
        val newTileuleft = Array.ofDim[Float](rows * columns, size * size)
        //val bitmaskuleft = Array.ofDim[Boolean](rows * columns)
        //util.Arrays.fill(bitmaskuleft,false)
        val bitmaskuleft = new BitArray(rows * columns)
        bitmaskuleft.fill(false)
        for(i <- x2 to x2) {
          for(j <- y2 to y2){
            util.Arrays.fill(newTileuleft(((i - x1) * tile.tileHeight) + (j - y1)), Float.NaN)
            var iter: Int = 8
            //ToDo:correct this
            for (k <- 1 to size / 2) {
              for(l <- 1 to size/2){
                if ((i + l) >= tile.x1 && (i + l) <= tile.x2 && (j + k) >= tile.y1 && (j + k) <= tile.y2) {
                  bitmaskuleft.set(((i - x1) * tile.tileHeight) + (j - y2),  bitmaskuleft.get(((i - x1) * tile.tileHeight) + (j - y2)) | tile.isEmpty(i + l, j + k))
                  if (!tile.isEmpty(i + l, j + k)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i + l, j + k)
                    newTileuleft(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                  iter = iter + 1
                }
              }
            }
          }
        }
        memoryTiles+= ((tilediagul,(9,new MemoryTile[Array[Float]](tilediagul, tile.rasterMetadata, numbands, newTileuleft, tile.minimum, tile.maximum, bitmaskuleft))))
      }

      if(tilediagur != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tilediagur)
        val x2 = tile.rasterMetadata.getTileX2(tilediagur)
        val y1 = tile.rasterMetadata.getTileY1(tilediagur)
        val y2 = tile.rasterMetadata.getTileY2(tilediagur)
        val newTileur = Array.ofDim[Float](rows * columns, size * size)
        val bitmaskur = new BitArray(rows * columns)
        bitmaskur.fill(false)

        for(i <- x1 to x1) {
          for(j <- y2 to y2){
            util.Arrays.fill(newTileur(((i - x1) * tile.tileHeight) + (j - y1)), Float.NaN)
            var iter: Int = 6
            //ToDo:correct this
            for (k <- 1 to size / 2) {
              for(l <- 1 to size/2){
                if ((i - l) >= tile.x1 && (i - l) <= tile.x2 && (j + k) >= tile.y1 && (j + k) <= tile.y2) {
                  bitmaskur.set(((i - x1) * tile.tileHeight) + (j - y2), bitmaskur.get(((i - x1) * tile.tileHeight) + (j - y2)) | tile.isEmpty(i - l, j + k))
                  if (!tile.isEmpty(i - l, j + k)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i - l, j + k)
                    newTileur(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                  // iter = iter + 1
                }
              }
            }
          }
        }
        memoryTiles+= ((tilediagur,(7,new MemoryTile[Array[Float]](tilediagur, tile.rasterMetadata, numbands, newTileur, tile.minimum, tile.maximum, bitmaskur))))
      }

      if(tilediagbl != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tilediagbl)
        val x2 = tile.rasterMetadata.getTileX2(tilediagbl)
        val y1 = tile.rasterMetadata.getTileY1(tilediagbl)
        val y2 = tile.rasterMetadata.getTileY2(tilediagbl)
        val newTilebleft = Array.ofDim[Float](rows * columns, size * size)
        val bitmaskbleft = new BitArray(rows * columns)
        bitmaskbleft.fill(false)

        for(i <- x2 to x2) {
          for(j <- y1 to y1){
            util.Arrays.fill(newTilebleft(((i - x1) * tile.tileHeight) + (j - y1)), Float.NaN)
            var iter: Int = 2
            //ToDo:correct this
            for (k <- 1 to size / 2) {
              for(l <- 1 to size/2){
                if ((i + l) >= tile.x1 && (i + l) <= tile.x2 && (j - k) >= tile.y1 && (j - k) <= tile.y2) {
                  bitmaskbleft.set(((i - x1) * tile.tileHeight) + (j - y2), bitmaskbleft.get(((i - x1) * tile.tileHeight) + (j - y2)) | tile.isEmpty(i + l, j - k))
                  if (!tile.isEmpty(i + l, j - k)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i + l, j - k)
                    newTilebleft(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                  // iter = iter + 1
                }
              }
            }
          }
        }
        memoryTiles+= ((tilediagbl,(3,new MemoryTile[Array[Float]](tilediagbl, tile.rasterMetadata, numbands, newTilebleft, tile.minimum, tile.maximum, bitmaskbleft))))
      }

      if(tilediagbr != Int.MinValue)
      {
        val x1 = tile.rasterMetadata.getTileX1(tilediagbr)
        val x2 = tile.rasterMetadata.getTileX2(tilediagbr)
        val y1 = tile.rasterMetadata.getTileY1(tilediagbr)
        val y2 = tile.rasterMetadata.getTileY2(tilediagbr)
        val newTilebr = Array.ofDim[Float](rows * columns, size * size)
        val bitmaskbr = new BitArray(rows * columns)
        bitmaskbr.fill(false)

        for(i <- x1 to x1) {
          for(j <- y1 to y1){
            util.Arrays.fill(newTilebr(((i - x1) * tile.tileHeight) + (j - y1)), Float.NaN)
            var iter: Int = 0
            //ToDo:correct this
            for (k <- 1 to size / 2) {
              for(l <- 1 to size/2){
                if ((i - l) >= tile.x1 && (i - l) <= tile.x2 && (j - k) >= tile.y1 && (j - k) <= tile.y2) {
                  bitmaskbr.set(((i - x1) * tile.tileHeight) + (j - y1), bitmaskbr.get(((i - x1) * tile.tileHeight) + (j - y1)) | tile.isEmpty(i - l, j - k))
                  if (!tile.isEmpty(i - l, j - k)) {
                    val pixelValues: Float = tile.getPixelValueAs[Float](i - l, j - k)
                    newTilebr(((i - x1) * tile.tileHeight) + (j - y1))(iter) = pixelValues
                  }
                  // iter = iter + 1
                }
              }
            }
          }
        }
        memoryTiles+= ((tilediagbr,(1,new MemoryTile[Array[Float]](tilediagbr, tile.rasterMetadata, numbands, newTilebr, tile.minimum, tile.maximum, bitmaskbr))))
      }


      memoryTiles
    })



    val reducedTiles : RDD[(Integer, MemoryTile[Array[Float]])] = partialTiles.groupByKey.map( partialtile => {

      val tileID = partialtile._1

      val tileIterator = partialtile._2

      var t_values = Array.ofDim[Float](1, size*size)
      var bitmask = new BitArray(1)
      var isSet : Boolean = false
      var rasterMetadata : RasterMetadata = null
      var numComponents : Int = 1
      var min : Float = 0
      var max : Float = 0

      for(tile <- tileIterator){
        val tileIDa = tile._1
        val tilea = tile._2
        if(!isSet)
        {
          isSet = true
          t_values = Array.ofDim[Float](tilea.tileHeight * tilea.tileWidth, size*size)
          bitmask = new BitArray(tilea.tileHeight * tilea.tileWidth)
          rasterMetadata = tilea.rasterMetadata
          numComponents = tilea.numComponents
          min = tilea.minimum
          max = tilea.maximum
        }

        if (tileIDa == 1) {
          for (i <- tilea.x1 to tilea.x1) {
            for (j <- tilea.y1 to tilea.y1) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1), bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1))  | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1))(0) = pixelValues(0)
              }
            }
          }
        }
        else if (tileIDa == 2) {

          bitmask.set((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1),  bitmask.get((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1)) | tilea.isEmpty(tilea.x1, tilea.y1))
          if (!tilea.isEmpty(tilea.x1, tilea.y1)) {
            val pixelValues: Array[Float] = tilea.getPixelValue(tilea.x1, tilea.y1)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(1) = pixelValues(1)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(2) = pixelValues(2)
          }

          for (i <- tilea.x1 + 1 to tilea.x2 - 1) {
            for(j <- tilea.y1 to tilea.y1){
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1), bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(0) = pixelValues(0)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(1) = pixelValues(1)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(2) = pixelValues(2)
              }
            }
          }
          bitmask.set((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1), bitmask.get((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1)) | tilea.isEmpty(tilea.x2, tilea.y1))
          if (!tilea.isEmpty(tilea.x2, tilea.y1)) {
            val pixelValues: Array[Float] = tilea.getPixelValue(tilea.x2, tilea.y1)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(0) = pixelValues(0)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(1) = pixelValues(1)
          }
        }
        else if (tileIDa == 3) {
          for(i <- tilea.x2 to tilea.x2) {
            for(j <- tilea.y1 to tilea.y1){
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1), bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1))  | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i , j )
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(2) = pixelValues(2)
              }
            }
          }
        }
        else if (tileIDa == 4) {
          bitmask.set((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1), bitmask.get((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1))| tilea.isEmpty(tilea.x1, tilea.y1))
          if (!tilea.isEmpty(tilea.x1, tilea.y1)) {
            val pixelValues: Array[Float] = tilea.getPixelValue(tilea.x1, tilea.y1)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(3) = pixelValues(3)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(6) = pixelValues(6)
          }
          for(i <- tilea.x1 to tilea.x1){
            for (j <- tilea.y1 + 1 to tilea.y2 - 1) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1),  bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(0) = pixelValues(0)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(3) = pixelValues(3)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(6) = pixelValues(6)
              }
            }
          }
          bitmask.set((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1),  bitmask.get((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1))  | tilea.isEmpty(tilea.x1, tilea.y2))
          if (!tilea.isEmpty(tilea.x1, tilea.y2)) {
            val pixelValues : Array[Float] = tilea.getPixelValue(tilea.x1, tilea.y2)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(0) = pixelValues(0)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(3) = pixelValues(3)
          }
        }
        else if (tileIDa == 5) {

          for(i <- tilea.x1 to tilea.x1){
            for (j <- tilea.y1 + 1 to tilea.y2 - 1) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1),  bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(1) = pixelValues(1)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(2) = pixelValues(2)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(4) = pixelValues(4)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(5) = pixelValues(5)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(7) = pixelValues(7)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(8) = pixelValues(8)
              }
            }
          }

          for(i <- tilea.x2 to tilea.x2){
            for (j <- tilea.y1 + 1 to tilea.y2 - 1) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1), bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(0) = pixelValues(0)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(1) = pixelValues(1)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(3) = pixelValues(3)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(4) = pixelValues(4)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(6) = pixelValues(6)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(7) = pixelValues(7)
              }
            }
          }

          for(i <- tilea.x1 + 1 to tilea.x2 - 1){
            for (j <- tilea.y1 to tilea.y1) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1), bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(3) = pixelValues(3)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(4) = pixelValues(4)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(5) = pixelValues(5)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(6) = pixelValues(6)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(7) = pixelValues(7)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(8) = pixelValues(8)
              }
            }
          }

          for(i <- tilea.x1 + 1 to tilea.x2 - 1){
            for (j <- tilea.y2 to tilea.y2) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1) ,bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1))| tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)

                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(0) = pixelValues(0)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(1) = pixelValues(1)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(2) = pixelValues(2)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(3) = pixelValues(3)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(4) = pixelValues(4)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(5) = pixelValues(5)
              }
            }
          }

          for (i <- tilea.x1 + 1 to tilea.x2 - 1) {
            for(j <- tilea.y1 + 1 to tilea.y2 - 1){
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1),bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(0) = pixelValues(0)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(1) = pixelValues(1)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(2) = pixelValues(2)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(3) = pixelValues(3)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(4) = pixelValues(4)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(5) = pixelValues(5)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(6) = pixelValues(6)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(7) = pixelValues(7)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(8) = pixelValues(8)
              }
            }
          }

          bitmask.set((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1),  bitmask.get((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1))  | tilea.isEmpty(tilea.x1, tilea.y1))
          if (!tilea.isEmpty(tilea.x1, tilea.y1)) {
            val pixelValues : Array[Float] = tilea.getPixelValue(tilea.x1, tilea.y1)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(4) = pixelValues(4)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(5) = pixelValues(5)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(7) = pixelValues(7)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(8) = pixelValues(8)
          }

          bitmask.set((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1), bitmask.get((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1)) | tilea.isEmpty(tilea.x1, tilea.y2))
          if (!tilea.isEmpty(tilea.x1, tilea.y2)) {
            val pixelValues : Array[Float] = tilea.getPixelValue(tilea.x1, tilea.y2)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(1) = pixelValues(1)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(2) = pixelValues(2)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(4) = pixelValues(4)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(5) = pixelValues(5)
          }

          bitmask.set((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1), bitmask.get((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1)) | tilea.isEmpty(tilea.x2, tilea.y1))
          if (!tilea.isEmpty(tilea.x2, tilea.y1)) {
            val pixelValues : Array[Float] = tilea.getPixelValue(tilea.x2, tilea.y1)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(3) = pixelValues(3)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(4) = pixelValues(4)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(6) = pixelValues(6)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(7) = pixelValues(7)
          }

          bitmask.set((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1), bitmask.get((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1)) | tilea.isEmpty(tilea.x2, tilea.y2))
          if (!tilea.isEmpty(tilea.x2, tilea.y2)) {
            val pixelValues : Array[Float] = tilea.getPixelValue(tilea.x2, tilea.y2)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(0) = pixelValues(0)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(1) = pixelValues(1)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(3) = pixelValues(3)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(4) = pixelValues(4)
          }

        }
        else if (tileIDa == 6) {
          bitmask.set((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1), bitmask.get((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y1 - tilea.y1)) | tilea.isEmpty(tilea.x2, tilea.y1))
          if (!tilea.isEmpty(tilea.x2, tilea.y1)) {
            val pixelValues: Array[Float] = tilea.getPixelValue(tilea.x2, tilea.y1)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(5) = pixelValues(5)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y1 - tilea.y1))(8) = pixelValues(8)
          }
          for(i <- tilea.x2 to tilea.x2){
            for (j <- tilea.y1 + 1 to tilea.y2 - 1) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1), bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(2) = pixelValues(2)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(5) = pixelValues(5)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(8) = pixelValues(8)
              }
            }
          }
          bitmask.set((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1), bitmask.get((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1)) | tilea.isEmpty(tilea.x2, tilea.y2))
          if (!tilea.isEmpty(tilea.x2, tilea.y2)) {
            val pixelValues : Array[Float] = tilea.getPixelValue(tilea.x2, tilea.y2)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(2) = pixelValues(2)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(5) = pixelValues(5)
          }
        }
        else if (tileIDa == 7) {
          for (i <- tilea.x1 to tilea.x1) {
            for (j <- tilea.y2 to tilea.y2) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1),  bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1))(6) = pixelValues(6)
              }
            }
          }
        }
        else if (tileIDa == 8) {
          bitmask.set((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1), bitmask.get((tilea.x1 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1))  | tilea.isEmpty(tilea.x1, tilea.y2))
          if (!tilea.isEmpty(tilea.x1, tilea.y2)) {
            val pixelValues: Array[Float] = tilea.getPixelValue(tilea.x1, tilea.y2)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(7) = pixelValues(7)
            t_values(((tilea.x1 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(8) = pixelValues(8)
          }

          for (i <- tilea.x1 + 1 to tilea.x2 - 1) {
            for(j <- tilea.y2 to tilea.y2){
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1),  bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(6) = pixelValues(6)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(7) = pixelValues(7)
                t_values(((i - tilea.x1) * tilea.tileHeight) + (j - tilea.y1))(8) = pixelValues(8)
              }
            }
          }

          bitmask.set((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1), bitmask.get((tilea.x2 - tilea.x1) * tilea.tileHeight + (tilea.y2 - tilea.y1)) | tilea.isEmpty(tilea.x2, tilea.y2))
          if (!tilea.isEmpty(tilea.x2, tilea.y2)) {
            val pixelValues: Array[Float] = tilea.getPixelValue(tilea.x2, tilea.y2)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(6) = pixelValues(6)
            t_values(((tilea.x2 - tilea.x1) * tilea.tileHeight) + (tilea.y2 - tilea.y1))(7) = pixelValues(7)
          }
        }
        else if (tileIDa == 9) {
          for (i <- tilea.x2 to tilea.x2) {
            for (j <- tilea.y2 to tilea.y2) {
              bitmask.set((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1),  bitmask.get((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1)) | tilea.isEmpty(i, j))
              if (!tilea.isEmpty(i, j)) {
                val pixelValues: Array[Float] = tilea.getPixelValue(i, j)
                t_values((i - tilea.x1) * tilea.tileHeight + (j - tilea.y1))(8) = pixelValues(8)
              }
            }
          }
        }
      }

      (tileID, new MemoryTile[Array[Float]](tileID, rasterMetadata, numComponents, t_values, min, max, bitmask))
    })

    val result : RDD[ITile] = reducedTiles.map(partialTile => {
      val tile = partialTile._2
      val tileID = partialTile._1
      val t_values = Array.ofDim[Float](tile.tileHeight * tile.tileWidth)
      val bitmask = new BitArray(tile.tileHeight * tile.tileWidth)

      for(i <- tile.x1 to tile.x2)
      {
        for(j <- tile.y1 to tile.y2)
        {
          bitmask.set((i - tile.x1) * tile.tileHeight + (j - tile.y1), bitmask.get((i - tile.x1) * tile.tileHeight + (j - tile.y1)) | tile.isEmpty(i, j))
          if(!tile.isEmpty(i, j)) {
            val pixelValues: Array[Float] = tile.getPixelValue(i, j)
            var sum: Float = 0
            for (k <- 0 until size * size) {
              sum = sum + pixelValues(k)
            }
            t_values((i - tile.x1) * tile.tileHeight + (j - tile.y1)) = sum / (size * size)
          }
        }
      }
      new MemoryTile[Float](tileID, tile.rasterMetadata, tile.numComponents, t_values, tile.minimum, tile.maximum, bitmask)
    })
    result
  }

  def mapPixels[U:ClassTag, T: ClassTag](raster:RDD[ITile], f:U => T): RDD[ITile]
  = {
    //try{raster.first().getPixelValueAs[U](raster.first().x1,raster.first().y1)}
    //catch exception
    //throw RuntimeException

    raster.map( tile => {
      val rows = tile.tileHeight
      val columns = tile.tileWidth
      val numbands = tile.numComponents

      val newTile = Array.ofDim[T](rows*columns)
      val bitmask = new BitArray(rows*columns)
      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2)
        {
          bitmask.set(((i-tile.x1)*tile.tileHeight)+(j-tile.y1), tile.isEmpty(i,j))
          if(!tile.isEmpty(i,j))
          {
            val pixelValues: U = tile.getPixelValueAs[U](i, j)
            newTile(((i-tile.x1)*tile.tileHeight)+(j-tile.y1)) = f(pixelValues)
          }
        }
      }
      new MemoryTile[T](tile.tileID, tile.rasterMetadata, numbands, newTile, tile.minimum, tile.maximum, bitmask)
    })
  }

  def histogram(raster: RDD[ITile]): RDD[((Int,Int), Long)] = {

    val partialhistogram :RDD[((Int,Int),Long)] = raster.flatMap(tile => {
      val numbands = tile.numComponents

      val histogramMap: mutable.HashMap[(Int, Int),Long] = new mutable.HashMap[(Int, Int),Long]
      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2)
        {
          if(!tile.isEmpty(i,j))
          {
            val pixelValue: Array[Int]  = tile.getPixelValuesAsInt(i, j)
            for( b <- 0 until  numbands)
           { if(histogramMap.contains((b,pixelValue(b))))
            {
              val value: Long = histogramMap.get((b,pixelValue(b))).get
              histogramMap.update((b,pixelValue(b)), value+1)
            }
            else
             histogramMap += ((b,pixelValue(b)) -> 1)
          }
          }
        }
      }
      histogramMap
    })
    partialhistogram.reduceByKey((a: Long, b: Long) => a+b )
  }

  def reprojection(raster: RDD[ITile], targetCRS: CoordinateReferenceSystem, numRows: Int, numCols: Int, tileWidth: Int, tileHeight: Int): RDD[ITile]= {

    val conf = raster.sparkContext.getConf
    val target_metadata : RasterMetadata = reproject_metadata_single(raster, targetCRS, numRows, numCols, tileWidth, tileHeight)
    val sourceCRS: CoordinateReferenceSystem = CRSServer.sridToCRS(raster.first().rasterMetadata.srid, conf)
    val transform: MathTransform = CRS.findMathTransform(sourceCRS, targetCRS, true)
    val transform_inverse: MathTransform = CRS.findMathTransform(targetCRS, sourceCRS, true)


    val partialTiles: RDD[(Integer, MemoryTile[Int])] = raster/*.repartition(numPartitions)*/.flatMap(sourceTile => {

      // System.out.println(target_metadata.x1 + "," + target_metadata.x2 + "," + target_metadata.y1 + "," + target_metadata.y2)

      val extent: util.ArrayList[Int] = reprojectExtent(sourceTile.rasterMetadata, target_metadata, sourceCRS, targetCRS, sourceTile.tileID, transform)
      //System.out.println("%%%%%%%%%%"+ extent.get(0)  + "," + extent.get(1) + "," + extent.get(2) + "," + extent.get(3))
      val targetRasterI1 = extent.get(0)
      val targetRasterI2 = Math.min(extent.get(1), target_metadata.x2 - 1)
      val targetRasterJ1 = extent.get(2)
      val targetRasterJ2 = Math.min(extent.get(3), target_metadata.y2 - 1)

      //System.out.println("%%%%%%%%%%"+ targetRasterI1  + "," + targetRasterI2 + "," + targetRasterJ1 + "," + targetRasterJ2)
      //assert(targetRasterI1 <= targetRasterI2)
      //assert(targetRasterJ1 <= targetRasterJ2)
      // TODO what is the correct data type of the target tile
      val memoryTiles = new scala.collection.mutable.ArrayBuffer[(Integer, MemoryTile[Int])]()
      var tileI1: Int = targetRasterI1
      var tileJ1: Int = targetRasterJ1
      // Convert to the model space in the source CRS
      while (tileI1 <= targetRasterI2 && tileJ1 <= targetRasterJ2) {
        // Get the end of the current tile (inclusive)
        val targetTileID = target_metadata.getTileIDAtPixel(tileI1, tileJ1)
        val tileI2: Int = target_metadata.getTileX2(targetTileID) min targetRasterI2
        val tileJ2: Int = target_metadata.getTileY2(targetTileID) min targetRasterJ2
        val t_values = new Array[Int](target_metadata.tileWidth * target_metadata.tileHeight)
        // val isEmpty = new Array[Boolean](tileHeight * tileWidth)
        //util.Arrays.fill(isEmpty, true)
        val isEmpty = new BitArray(tileHeight * tileWidth)
        util.Arrays.fill(t_values, Int.MinValue)
        isEmpty.fill(true)
        val targetTile = new MemoryTile[Int](targetTileID, target_metadata, 1, t_values, sourceTile.minimum, sourceTile.maximum, isEmpty)
        var isBlank: Boolean = true
        for (targetI <- tileI1 to tileI2) {
          for (targetJ <- tileJ1 to tileJ2) {
            // Get the value of the pixel from the source data
            // Find the model space in the target CRS of the center of the target pixel
            val t_mpt: DirectPosition2D = new DirectPosition2D()
            target_metadata.gridToModel(targetI + 0.5, targetJ + 0.5, t_mpt)
            val s_mpt: DirectPosition2D = new DirectPosition2D()
            transform_inverse.transform(t_mpt, s_mpt)
            // Find the grid coordinates in the source raster
            val s_gpt: Point2D.Double = new Point2D.Double()
            sourceTile.rasterMetadata.modelToGrid(s_mpt.getX, s_mpt.getY, s_gpt)
            // If everything goes right, the source pixel must be within the coordinates of the source tile
            val sourceI = s_gpt.getX.toInt
            val sourceJ = s_gpt.getY.toInt
            if (sourceI >= sourceTile.x1 && sourceI <= sourceTile.x2 && sourceJ >= sourceTile.y1 && sourceJ <= sourceTile.y2) {
              if (!sourceTile.isEmpty(sourceI, sourceJ)) {
                targetTile.setPixel(targetI, targetJ, sourceTile.getPixelValuesAsInt(sourceI, sourceJ)(0))
                isBlank = false
              }
            }
          }
        }
        // Add the tile to the in-memory tiles
        if (!isBlank) {
          targetTile.compress()
          memoryTiles += ((targetTileID, targetTile))
        }
        // Move to the next tile in the same row
        tileI1 = tileI2 + 1
        if (tileI1 >= targetRasterI2) {
          // Reached end of current row, move to the next line
          tileI1 = targetRasterI1
          tileJ1 = tileJ2 + 1
        }
      }
      //System.out.println("@@@@@@@@@@@@@@@@@"+memoryTiles.size)
      memoryTiles
    })
    // System.out.println("######################" + partialTiles.count())

    val reducedTiles: RDD[(Integer, MemoryTile[Int])] = partialTiles/*.repartitionAndSortWithinPartitions(new HashPartitioner(target_metadata.numTiles))*/.reduceByKey((memoryTilea, memoryTileb) => {
      val t_values = new Array[Int](memoryTilea.tileHeight * memoryTilea.tileWidth)
      val bitmask = new BitArray(tileHeight * tileWidth)
      bitmask.fill(true)
      util.Arrays.fill(t_values, Int.MinValue)
      //val bitmask = new Array[Boolean](tileHeight * tileWidth)
      //util.Arrays.fill(bitmask, true)
      val result = new MemoryTile[Int](memoryTilea.tileID, memoryTilea.rasterMetadata, memoryTilea.numComponents, t_values, memoryTilea.minimum, memoryTilea.maximum, bitmask)
      for (i <- memoryTilea.x1 to memoryTilea.x2) {
        for (j <- memoryTilea.y1 to memoryTilea.y2) {
          if (!memoryTilea.isEmpty(i, j) && !memoryTileb.isEmpty(i, j)) {
            result.setPixel(i,j, ((memoryTilea.getPixelValue(i, j) + memoryTileb.getPixelValue(i, j)) / 2))
          }
          else if (!memoryTilea.isEmpty(i, j)) {
            result.setPixel(i,j, memoryTilea.getPixelValue(i, j))
          }
          else if (!memoryTileb.isEmpty(i, j)) {
            result.setPixel(i,j, memoryTileb.getPixelValue(i, j))
          }
        }
      }
      result.compress()
      result
    })
    reducedTiles.map(x => x._2)
  }

  def reproject_metadata(raster: RDD[ITile], targetCRS: CoordinateReferenceSystem, numRows: Int, numCols: Int, tileWidth: Int, tileHeight: Int): RasterMetadata =
  {
    val source_metadata= raster.map(tile => tile.rasterMetadata).collect()

    val ul: Point2D.Double = new Point2D.Double(0, 0)
    val lr: Point2D.Double = new Point2D.Double(0, 0)
    val ll: Point2D.Double = new Point2D.Double(0, 0)
    val ur: Point2D.Double = new Point2D.Double(0, 0)

    for( metadata <- source_metadata)
    {
      val sul: Point2D.Double = new Point2D.Double()
      val slr: Point2D.Double = new Point2D.Double()
      val sll: Point2D.Double = new Point2D.Double()
      val sur: Point2D.Double = new Point2D.Double()
      metadata.gridToModel(metadata.x1, metadata.y1, sul)
      metadata.gridToModel(metadata.x2 - 1, metadata.y2 - 1, slr)
      metadata.gridToModel(metadata.x1, metadata.y2-1, sll)
      metadata.gridToModel(metadata.x2-1, metadata.y1, sur)

      lr.y = Math.min(lr.y, slr.getY)
      lr.x = Math.max(lr.x, slr.getX)
      ul.y = Math.max(ul.y, sul.getY)
      ul.x = Math.min(ul.x, sul.getX)

      ll.y = Math.min(ll.y, sll.getY)
      ll.x = Math.min(ll.x, sll.getX)
      ur.y = Math.max(ur.y, sur.getY)
      ur.x = Math.max(ur.x, sur.getX)
    }

//    if(targetCRS.getDomainOfValidity!=null) {
//      val targetCRSExtent = targetCRS.getDomainOfValidity.getGeographicElements.iterator()
//      val targetCRSbbox = targetCRSExtent.next().asInstanceOf[GeographicBoundingBox]
//      lr.y = if(lr.getY > targetCRSbbox.getSouthBoundLatitude) lr.getY else targetCRSbbox.getSouthBoundLatitude
//      lr.x = if(lr.getX < targetCRSbbox.getEastBoundLongitude) lr.getX else targetCRSbbox.getEastBoundLongitude
//      ul.y = if(ul.getY < targetCRSbbox.getNorthBoundLatitude) ul.getY else targetCRSbbox.getNorthBoundLatitude
//      ul.x = if(ul.getX > targetCRSbbox.getWestBoundLongitude) ul.getX else targetCRSbbox.getWestBoundLongitude
//
//      ll.y = if(ll.getY > targetCRSbbox.getSouthBoundLatitude) ll.getY else targetCRSbbox.getSouthBoundLatitude
//      ll.x = if(ll.getX > targetCRSbbox.getWestBoundLongitude) ll.getX else targetCRSbbox.getWestBoundLongitude
//      ur.y = if(ur.getY < targetCRSbbox.getNorthBoundLatitude) ur.getY else targetCRSbbox.getNorthBoundLatitude
//      ur.x = if(ur.getX < targetCRSbbox.getEastBoundLongitude) ur.getX else targetCRSbbox.getEastBoundLongitude
//    }

    val sourceCRS: CoordinateReferenceSystem = CRSServer.sridToCRS(source_metadata(0).srid, new SparkConf())
    val transform = CRS.findMathTransform(sourceCRS, targetCRS,true)

    val tul = new DirectPosition2D(targetCRS)
    val tlr = new DirectPosition2D(targetCRS)
    val tur = new DirectPosition2D(targetCRS)
    val tll = new DirectPosition2D(targetCRS)

    transform.transform(new DirectPosition2D(sourceCRS,Math.max(ul.getX,-180.0),ul.getY), tul)
    transform.transform(new DirectPosition2D(sourceCRS,Math.max(lr.getX,-180.0),lr.getY), tlr)
    transform.transform(new DirectPosition2D(sourceCRS,Math.max(ur.getX,-180.0),ur.getY), tur)
    transform.transform(new DirectPosition2D(sourceCRS,Math.max(ll.getX,-180.0),ll.getY), tll)

    val minX : Double = Math.min(Math.min(tll.getX, tlr.getX),Math.min(tul.getX,tur.getX))
    val maxX : Double = Math.max(Math.max(tll.getX, tlr.getX),Math.max(tul.getX,tur.getX))
    val minY : Double = Math.min(Math.min(tll.getY, tlr.getY),Math.min(tul.getY,tur.getY))
    val maxY : Double = Math.max(Math.max(tll.getY, tlr.getY),Math.max(tul.getY,tur.getY))


    val target_m2g: AffineTransform = new AffineTransform(1, 0, 0, 1, 0, 0)
    target_m2g.concatenate(AffineTransform.getScaleInstance((numRows - 1) / (maxX - minX ), (numCols - 1) / (minY - maxY)))
    target_m2g.concatenate(AffineTransform.getTranslateInstance(-minX, -maxY))
    new RasterMetadata(0, 0, numRows, numCols, tileWidth, tileHeight, CRSServer.crsToSRID(targetCRS, new SparkConf()), target_m2g.createInverse())
  }

  def reproject_metadata_single(raster: RDD[ITile], targetCRS: CoordinateReferenceSystem, numRows: Int, numCols: Int, tileWidth: Int, tileHeight: Int): RasterMetadata =
  {
    val source_metadata= raster.first().rasterMetadata

      val sul: Point2D.Double = new Point2D.Double()
      val slr: Point2D.Double = new Point2D.Double()
      val sll: Point2D.Double = new Point2D.Double()
      val sur: Point2D.Double = new Point2D.Double()
      source_metadata.gridToModel(source_metadata.x1, source_metadata.y1, sul)
      source_metadata.gridToModel(source_metadata.x2 - 1, source_metadata.y2 - 1, slr)
      source_metadata.gridToModel(source_metadata.x1, source_metadata.y2-1, sll)
      source_metadata.gridToModel(source_metadata.x2-1,source_metadata.y1, sur)


    //    if(targetCRS.getDomainOfValidity!=null) {
    //      val targetCRSExtent = targetCRS.getDomainOfValidity.getGeographicElements.iterator()
    //      val targetCRSbbox = targetCRSExtent.next().asInstanceOf[GeographicBoundingBox]
    //      lr.y = if(lr.getY > targetCRSbbox.getSouthBoundLatitude) lr.getY else targetCRSbbox.getSouthBoundLatitude
    //      lr.x = if(lr.getX < targetCRSbbox.getEastBoundLongitude) lr.getX else targetCRSbbox.getEastBoundLongitude
    //      ul.y = if(ul.getY < targetCRSbbox.getNorthBoundLatitude) ul.getY else targetCRSbbox.getNorthBoundLatitude
    //      ul.x = if(ul.getX > targetCRSbbox.getWestBoundLongitude) ul.getX else targetCRSbbox.getWestBoundLongitude
    //
    //      ll.y = if(ll.getY > targetCRSbbox.getSouthBoundLatitude) ll.getY else targetCRSbbox.getSouthBoundLatitude
    //      ll.x = if(ll.getX > targetCRSbbox.getWestBoundLongitude) ll.getX else targetCRSbbox.getWestBoundLongitude
    //      ur.y = if(ur.getY < targetCRSbbox.getNorthBoundLatitude) ur.getY else targetCRSbbox.getNorthBoundLatitude
    //      ur.x = if(ur.getX < targetCRSbbox.getEastBoundLongitude) ur.getX else targetCRSbbox.getEastBoundLongitude
    //    }

    val sourceCRS: CoordinateReferenceSystem = CRSServer.sridToCRS(source_metadata.srid, new SparkConf())
    val transform = CRS.findMathTransform(sourceCRS, targetCRS,true)

    val tul = new DirectPosition2D(targetCRS)
    val tlr = new DirectPosition2D(targetCRS)
    val tur = new DirectPosition2D(targetCRS)
    val tll = new DirectPosition2D(targetCRS)

    transform.transform(new DirectPosition2D(sourceCRS,Math.max(sul.getX,-180.0),sul.getY), tul)
    transform.transform(new DirectPosition2D(sourceCRS,Math.max(slr.getX,-180.0),slr.getY), tlr)
    transform.transform(new DirectPosition2D(sourceCRS,Math.max(sur.getX,-180.0),sur.getY), tur)
    transform.transform(new DirectPosition2D(sourceCRS,Math.max(sll.getX,-180.0),sll.getY), tll)

    val minX : Double = Math.min(Math.min(tll.getX, tlr.getX),Math.min(tul.getX,tur.getX))
    val maxX : Double = Math.max(Math.max(tll.getX, tlr.getX),Math.max(tul.getX,tur.getX))
    val minY : Double = Math.min(Math.min(tll.getY, tlr.getY),Math.min(tul.getY,tur.getY))
    val maxY : Double = Math.max(Math.max(tll.getY, tlr.getY),Math.max(tul.getY,tur.getY))


    val target_m2g: AffineTransform = new AffineTransform(1, 0, 0, 1, 0, 0)
    target_m2g.concatenate(AffineTransform.getScaleInstance((numRows - 1) / (maxX - minX ), (numCols - 1) / (minY - maxY)))
    target_m2g.concatenate(AffineTransform.getTranslateInstance(-minX, -maxY))
    new RasterMetadata(0, 0, numRows, numCols, tileWidth, tileHeight, CRSServer.crsToSRID(targetCRS, new SparkConf()), target_m2g.createInverse())
  }

  def reprojectExtent(srasterMetadata: RasterMetadata, trasterMetadata: RasterMetadata, sourceCRS: CoordinateReferenceSystem, targetCRS: CoordinateReferenceSystem, tileID: Int, transform: MathTransform): util.ArrayList[Int] = {
    val sul: Point2D.Double = new Point2D.Double()
    val slr: Point2D.Double = new Point2D.Double()
    val sll: Point2D.Double = new Point2D.Double()
    val sur: Point2D.Double = new Point2D.Double()
    // Model coordinates of the upper-left corner of the upper-left pixel in the source raster and source CRS
    srasterMetadata.gridToModel(srasterMetadata.getTileX1(tileID), srasterMetadata.getTileY1(tileID), sul)
    // Model coordinates of the lower-left corner of the lower-left pixel in the source raster and source CRS
    srasterMetadata.gridToModel(srasterMetadata.getTileX1(tileID), srasterMetadata.getTileY2(tileID) + 1, sll)
    // Model coordinates of the upper-right corner of the upper-right pixel in the source raster and source CRS
    srasterMetadata.gridToModel(srasterMetadata.getTileX2(tileID) + 1, srasterMetadata.getTileY1(tileID), sur)
    // Model coordinates of the lower-right corner of the lower-right pixel in the source raster and source CRS
    srasterMetadata.gridToModel(srasterMetadata.getTileX2(tileID) + 1, srasterMetadata.getTileY2(tileID) + 1, slr)

//        if (targetCRS.getDomainOfValidity != null) {
//          val targetCRSExtent = targetCRS.getDomainOfValidity.getGeographicElements.iterator()
//          val targetCRSbbox = targetCRSExtent.next().asInstanceOf[GeographicBoundingBox]
//          slr.y = Math.min(Math.max(slr.getY, targetCRSbbox.getSouthBoundLatitude), targetCRSbbox.getNorthBoundLatitude)
//          slr.x = Math.max(Math.min(slr.getX, targetCRSbbox.getEastBoundLongitude), targetCRSbbox.getWestBoundLongitude)
//          sul.y = Math.max(Math.min(sul.getY, targetCRSbbox.getNorthBoundLatitude), targetCRSbbox.getSouthBoundLatitude)
//          sul.x = Math.min(Math.max(sul.getX, targetCRSbbox.getWestBoundLongitude), targetCRSbbox.getEastBoundLongitude)
//
//          sll.y = Math.min(Math.max(sll.getY, targetCRSbbox.getSouthBoundLatitude), targetCRSbbox.getNorthBoundLatitude)
//          sll.x = Math.min(Math.max(sll.getX, targetCRSbbox.getWestBoundLongitude), targetCRSbbox.getEastBoundLongitude)
//          sur.y = Math.max(Math.min(sur.getY, targetCRSbbox.getNorthBoundLatitude), targetCRSbbox.getSouthBoundLatitude)
//          sur.x = Math.max(Math.min(sur.getX, targetCRSbbox.getEastBoundLongitude), targetCRSbbox.getWestBoundLongitude)
//        }

    // Convert the four corners to the model coordinates of the target CRS
    val tul = new DirectPosition2D()
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(sul.getX, -180.0), sul.getY), tul)
    val tlr = new DirectPosition2D()
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(slr.getX, -180.0), slr.getY), tlr)
    val tur = new DirectPosition2D()
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(sur.getX, -180.0), sur.getY), tur)
    val tll = new DirectPosition2D()
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(sll.getX, -180.0), sll.getY), tll)

    val ttile_ul: Point2D.Double = new Point2D.Double()
    val ttile_lr: Point2D.Double = new Point2D.Double()
    val ttile_ur: Point2D.Double = new Point2D.Double()
    val ttile_ll: Point2D.Double = new Point2D.Double()

    trasterMetadata.modelToGrid(tul.getX, tul.getY, ttile_ul)
    trasterMetadata.modelToGrid(tlr.getX, tlr.getY, ttile_lr)
    trasterMetadata.modelToGrid(tur.getX, tur.getY, ttile_ur)
    trasterMetadata.modelToGrid(tll.getX, tll.getY, ttile_ll)

    val targetRasterI1: Int = Math.max(0, Math.round(Math.min(Math.min(ttile_ul.getX, ttile_ll.getX), Math.min(ttile_ur.getX, ttile_lr.getX))).toInt)
    val targetRasterJ1: Int = Math.max(0, Math.round(Math.min(Math.min(ttile_ul.getY, ttile_ll.getY), Math.min(ttile_ur.getY, ttile_lr.getY))).toInt)
    val targetRasterI2: Int = Math.max(0, Math.round(Math.max(Math.max(ttile_ul.getX, ttile_ll.getX), Math.max(ttile_ur.getX, ttile_lr.getX))).toInt - 1)
    val targetRasterJ2: Int = Math.max(0, Math.round(Math.max(Math.max(ttile_ul.getY, ttile_ll.getY), Math.max(ttile_ur.getY, ttile_lr.getY))).toInt - 1)

    val extent: util.ArrayList[Int] = new util.ArrayList[Int](4)
    extent.add(targetRasterI1)
    extent.add(targetRasterI2)
    extent.add(targetRasterJ1)
    extent.add(targetRasterJ2)
    extent
  }

  //ToDo: remove hardcoded -180.0 for WGS84
  def metadata(rasterMetadata: RasterMetadata, sourceCRS: CoordinateReferenceSystem, targetCRS: CoordinateReferenceSystem, numRows: Int, numCols: Int, tileWidth: Int, tileHeight: Int, transform: MathTransform): RasterMetadata = {
    val sul: Point2D.Double = new Point2D.Double()
    val slr: Point2D.Double = new Point2D.Double()
    val sll: Point2D.Double = new Point2D.Double()
    val sur: Point2D.Double = new Point2D.Double()
    rasterMetadata.gridToModel(rasterMetadata.x1, rasterMetadata.y1, sul)
    rasterMetadata.gridToModel(rasterMetadata.x2 - 1, rasterMetadata.y2 - 1, slr)
    rasterMetadata.gridToModel(rasterMetadata.x1, rasterMetadata.y2 - 1, sll)
    rasterMetadata.gridToModel(rasterMetadata.x2 - 1, rasterMetadata.y1, sur)

    // val transform = CRS.findMathTransform(sourceCRS, targetCRS, true)
    val tul = new DirectPosition2D(targetCRS)
    val tlr = new DirectPosition2D(targetCRS)
    val tur = new DirectPosition2D(targetCRS)
    val tll = new DirectPosition2D(targetCRS)

//        if (targetCRS.getDomainOfValidity != null) {
//          val targetCRSExtent = targetCRS.getDomainOfValidity.getGeographicElements.iterator()
//          val targetCRSbbox = targetCRSExtent.next().asInstanceOf[GeographicBoundingBox]
//          slr.y = if (slr.getY > targetCRSbbox.getSouthBoundLatitude) slr.getY else targetCRSbbox.getSouthBoundLatitude
//          slr.x = if (slr.getX < targetCRSbbox.getEastBoundLongitude) slr.getX else targetCRSbbox.getEastBoundLongitude
//          sul.y = if (sul.getY < targetCRSbbox.getNorthBoundLatitude) sul.getY else targetCRSbbox.getNorthBoundLatitude
//          sul.x = if (sul.getX > targetCRSbbox.getWestBoundLongitude) sul.getX else targetCRSbbox.getWestBoundLongitude
//
//          sll.y = if (sll.getY > targetCRSbbox.getSouthBoundLatitude) sll.getY else targetCRSbbox.getSouthBoundLatitude
//          sll.x = if (sll.getX > targetCRSbbox.getWestBoundLongitude) sll.getX else targetCRSbbox.getWestBoundLongitude
//          sur.y = if (sur.getY < targetCRSbbox.getNorthBoundLatitude) sur.getY else targetCRSbbox.getNorthBoundLatitude
//          sur.x = if (sur.getX < targetCRSbbox.getEastBoundLongitude) sur.getX else targetCRSbbox.getEastBoundLongitude
//        }

    transform.transform(new DirectPosition2D(sourceCRS, Math.max(sul.getX, -180.0), sul.getY), tul)
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(slr.getX, -180.0), slr.getY), tlr)
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(sur.getX, -180.0), sur.getY), tur)
    transform.transform(new DirectPosition2D(sourceCRS, Math.max(sll.getX, -180.0), sll.getY), tll)

    val minX: Double = Math.min(Math.min(tll.getX, tlr.getX), Math.min(tul.getX, tur.getX))
    val maxX: Double = Math.max(Math.max(tll.getX, tlr.getX), Math.max(tul.getX, tur.getX))
    val minY: Double = Math.min(Math.min(tll.getY, tlr.getY), Math.min(tul.getY, tur.getY))
    val maxY: Double = Math.max(Math.max(tll.getY, tlr.getY), Math.max(tul.getY, tur.getY))

    //mirror
    val target_m2g: AffineTransform = new AffineTransform(1, 0, 0, 1, 0, 0)
    //target_m2g.concatenate(AffineTransform.getScaleInstance((numRows - 1) / (tlr.getX - tul.getX ), (numCols - 1) / (tlr.getY - tul.getY)))
    //ToDo: Check if x,y axis direction is different for some CRS
    target_m2g.concatenate(AffineTransform.getScaleInstance((numRows - 1) / (maxX - minX), (numCols - 1) / (minY - maxY)))
    //scale
    target_m2g.concatenate(AffineTransform.getTranslateInstance(-minX, -maxY))
    //target_m2g.concatenate(AffineTransform.getTranslateInstance(-tul.getX, -tul.getY))
    //translate
    new RasterMetadata(0, 0, numRows, numCols, tileWidth, tileHeight, CRSServer.crsToSRID(targetCRS, new SparkConf()), target_m2g.createInverse())

  }


//  def maxThreshold(raster : RDD[ITile], value: Int): RDD[ITile] = {
//    raster.map( tile => {
//      val rows = tile.tileHeight
//      val columns = tile.tileWidth
//      val numbands = tile.numComponents
//
//      val values = new Array[Array[Int]](rows*columns)
//      val isEmpty = new BitArray(rows*columns)
//      isEmpty.fill(true)
//      val result = new MemoryTile[Array[Int]](tile.tileID, tile.rasterMetadata, numbands, values, tile.minimum, value, isEmpty)
//      for (i <- tile.x1 to tile.x2)
//        for (j <- tile.y1 to tile.y2) {
//          values((i-tile.x1)*(tile.y2 - tile.y1) + (j - tile.y1)) = new Array[Int](numbands)
//          util.Arrays.fill(values((i-tile.x1)*(tile.y2 - tile.y1) + (j - tile.y1)), Int.MinValue)
//          if(!tile.isEmpty(i,j)) {
//            val pixelValue: Array[Int] = tile.getPixelValuesAsInt(i, j)
//            for(b <- 0 until numbands) {
//              if (pixelValue(b) > value)
//                pixelValue(b) = 0
//            }
//            result.setPixel(i, j, pixelValue)
//          }
//        }
//      result
//    })
//  }
  def maxThreshold(raster : RDD[ITile], value: Int): RDD[ITile] = {
      raster.map( tile => {
        val rows = tile.tileHeight
        val columns = tile.tileWidth
        val numbands = tile.numComponents

        val values = new Array[Int](rows*columns)
        val isEmpty = new BitArray(rows*columns)
        util.Arrays.fill(values, Int.MinValue)
        isEmpty.fill(true)
        val result = new MemoryTile[Int](tile.tileID, tile.rasterMetadata, numbands, values, tile.minimum, value, isEmpty)
        for (i <- tile.x1 to tile.x2)
            for (j <- tile.y1 to tile.y2) {
              if(!tile.isEmpty(i,j)) {
                val pixelValue: Int = tile.getPixelValueAsInt(i, j)
                if(pixelValue > value)
                  result.setPixel(i, j, 0)
                else
                  result.setPixel(i, j, pixelValue)
              }
            }
        result
      })
    }
    def minThreshold(raster : RDD[ITile], value: Float): RDD[ITile] = {
      raster.map( tile => {
        val rows = tile.tileHeight
        val columns = tile.tileWidth
        val numbands = tile.numComponents

        val values = new Array[Float](rows*columns)
        val isEmpty = new BitArray(rows*columns)
        util.Arrays.fill(values, Float.MinValue)
        isEmpty.fill(true)
        val result = new MemoryTile[Float](tile.tileID, tile.rasterMetadata, numbands, values, value, tile.maximum, isEmpty)
        for (i <- tile.x1 to tile.x2)
          for (j <- tile.y1 to tile.y2) {
            if (!tile.isEmpty(i, j)) {
              val pixelValue: Float = tile.getPixelValueAsFloat(i, j)
              if (pixelValue < value)
                result.setPixel(i, j, 0)
              else
                result.setPixel(i, j, pixelValue)
            }
          }
        result
      })
    }

  //  def threshold(raster : RDD[ITile], value1: Float, value2: Float): RDD[ITile] = {
  //    raster.map( tile => {
  //      val rows = tile.tileHeight
  //      val columns = tile.tileWidth
  //      val numbands = tile.numComponents
  //      val values = new Array[Float](rows*columns)
  //      val isEmpty = new BitArray(rows*columns)
  //      util.Arrays.fill(values, Float.MinValue)
  //      isEmpty.fill(true)
  //      val result = new MemoryTile[Float](tile.tileID, tile.rasterMetadata, numbands, values, value1,  value2, isEmpty)
  //
  //      for (i <- tile.x1 to tile.x2)
  //        for (j <- tile.y1 to tile.y2) {
  //          if(tile.isEmpty(i,j)){
  //            val pixelValue: Float = tile.getPixelValueAsFloat(i, j)
  //            if (pixelValue < value1 || pixelValue > value2)
  //              result.setPixel(i, j, 0)
  //            else
  //              result.setPixel(i, j, pixelValue)
  //          }
  //        }
  //      result
  //    })
  //  }

  def maximum(raster : RDD[ITile]): Float ={
    raster.map(tile => {
      var max:Float = tile.getPixelValueAsFloat(tile.x1, tile.y1)
      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2) {
          val value:Float = tile.getPixelValueAsFloat(i, j)
          if(max < value)
            max = value
        }
      }
      max
    }).reduce((a,b) => a max b)
  }

  def stats(raster : RDD[ITile]): (Array[Int], Array[Int], Array[Long], Array[Long]) ={
    raster.map(tile => {
      val numbands = tile.numComponents
      val max:Array[Int] = new Array[Int](numbands)
      val min:Array[Int] = new Array[Int](numbands)
      val sum:Array[Long] = new Array[Long](numbands)
      val count: Array[Long] = new Array[Long](numbands)
      for(b <- 0 until numbands)
        {
          max(b) = Int.MinValue
          min(b) = Int.MaxValue
          count(b) = 0
          sum(b) = 0
        }
      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2) {
          if(!tile.isEmpty(i,j))
          {
            val value:Array[Int] = tile.getPixelValuesAsInt(i, j)
            for( b <- 0 until numbands){
              if(max(b) < value(b))
                max(b) = value(b)
              if(min(b) > value(b))
                min(b) = value(b)
              sum(b) += value(b)
              count(b) +=1
            }

        }
        }
      }
      (max,min,sum, count)
    }).reduce((a,b) => {
      for(i <- 0 until a._1.length)
        {
          a._1(i) = a._1(i) max b._1(i)
          a._2(i) = a._2(i) min b._2(i)
          a._3(i) = a._3(i) + b._3(i)
          a._4(i) = a._4(i) + b._4(i)
        }
      a})
  }

  def stats_singleband(raster : RDD[ITile]): (Float, Float, Float, Long) ={
    raster.map(tile => {
      var max: Float = Float.MinValue
      var min: Float = Float.MaxValue
      var sum: Float = 0
      var count: Long = 0

      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2) {
          if(!tile.isEmpty(i,j))
          {
            val value: Float = tile.getPixelValueAsFloat(i, j)
              if(max < value)
                max = value
              if(min > value)
                min = value
              sum += value
              count += 1
            }

          }
        }
      (max,min,sum, count)

    }).reduce((a,b) => (a._1 max b._1, a._2 min b._2, a._3 + b._3, a._4 + b._4))
  }


  def std_dev_singleband(raster: RDD[ITile], mean: Float, count: Long): Double = {
    val sum: Float = raster.map(tile => {
      var std_dev_sum: Float = 0

      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2) {
          if (!tile.isEmpty(i, j)) {
            val value: Float = tile.getPixelValueAsFloat(i, j)
            std_dev_sum += ((value - mean) * (value - mean))
          }

        }
      }
      std_dev_sum

    }).reduce((a, b) => (a + b))
    Math.sqrt(sum / count)
  }

  def std_dev(raster: RDD[ITile], mean: Array[Float], count: Array[Long]): Array[Double] = {
    val sum: Array[Float] = raster.map(tile => {
      val numbands = tile.numComponents
      val std_dev_sum: Array[Float] = new Array[Float](numbands)
      for (b <- 0 until numbands)
        std_dev_sum(b) = 0

      for (i <- tile.x1 to tile.x2) {
        for (j <- tile.y1 to tile.y2) {
          if (!tile.isEmpty(i, j)) {
            val value: Array[Int] = tile.getPixelValuesAsInt(i, j)
            for (b <- 0 until numbands)
              std_dev_sum(b) += ((value(b) - mean(b)) * (value(b) - mean(b)))
          }

        }
      }
      std_dev_sum

    }).reduce((a, b) => {
      for (i <- 0 until a.length)
        a(i) = a(i) + b(i)
      a
    })
    val std_dev_array = new Array[Double](sum.length)
    for (i <- 0 until sum.length)
      std_dev_array(i) = Math.sqrt(sum(i) / count(i))
    std_dev_array
  }

  def minimum(raster : RDD[ITile]):Int={
    raster.map(tile => {
      var min: Int = Int.MaxValue
      for (i <- tile.x1 to tile.x2 ) {
        for (j <- tile.y1 to tile.y2) {
          val value: Int = tile.getPixelValuesAsInt(i, j)(0)
          if(min > value)
            min = value
        }
      }
      min
    }).reduce((a,b) => a min b)
  }

  def minimum_multi(raster : RDD[ITile]):Array[Int]={
    raster.map(tile => {
      val numbands = tile.numComponents
      var min: Array[Int] = new Array[Int](numbands)
      for(b<- 0 until numbands)
        min(b) = Int.MinValue
      for (i <- tile.x1 to tile.x2 ) {
        for (j <- tile.y1 to tile.y2) {
          val value: Array[Int] = tile.getPixelValuesAsInt(i, j)
          for(b <- 0 until( numbands) )
          {
          if(min(b) > value(b))
            min(b) = value(b)
          }
        }
      }
      min
    }).reduce((a,b) => {
      for(i <- 0 until a.length) {
      a(i) = a(i) min b(i)
      }
      a
    })
  }

}
