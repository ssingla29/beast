/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor

import edu.ucr.cs.bdlab.beast.cg.CGOperationsMixin._
import edu.ucr.cs.bdlab.beast.cg.SpatialDataTypes.SpatialRDD
import edu.ucr.cs.bdlab.beast.common.{BeastOptions, CLIOperation}
import edu.ucr.cs.bdlab.beast.geolite.{Feature, IFeature, ITile}
import edu.ucr.cs.bdlab.beast.io.ReadWriteMixin._
import edu.ucr.cs.bdlab.beast.io.{CSVFeatureWriter, SpatialFileRDD, SpatialOutputFormat, SpatialWriter}
import edu.ucr.cs.bdlab.beast.util.{OperationMetadata, OperationParam}
import org.apache.hadoop.fs.{Path, PathFilter}
import org.apache.spark.beast.CRSServer
import org.apache.spark.beast.sql.GeometryDataType
import org.apache.spark.internal.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.{DoubleType, StructField, StructType}
import org.apache.spark.util.LongAccumulator
import org.apache.spark.{SparkConf, SparkContext}

import java.io.{IOException, PrintWriter}
import java.util

@OperationMetadata(shortName = "zs",
  description = "Computes zonal statistics between a vector file and a raster file. Input files (vector, raster)",
  inputArity = "2",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialFileRDD], classOf[SpatialOutputFormat]))
object ZonalStatistics extends CLIOperation with Logging {

  @OperationParam(description = "The statistics function to compute {aggregates}", defaultValue = "aggregates")
  val AggregateFunction = "collector"

  @OperationParam(description = "The name or the index of the raster layer to read from the raster file", defaultValue = "0")
  val RasterLayer = "layer"

  @OperationParam(description = "The method to use with RaptorJoin {file,rdd}", defaultValue = "file")
  val RJMethod = "rjmethod"

  /**
   * Extract the spatial reference identifier from a raster file. This method is used to reproject vector data to the
   * correct CRS of the raster data.
   * @param rasterPath path to the raster files
   * @param opts additional options passed to the raster reader
   * @param conf Spark configuration that is used to access the CRS server to retrieve the SRID
   * @return a unique ID of the CRS in the first raster file located in the given directory
   */
  def getRasterSRID(rasterPath: Path, opts: BeastOptions, conf: SparkConf): Int = {
    val rasterFileSystem = rasterPath.getFileSystem(opts.loadIntoHadoopConf(null))
    // Store raster files as a list of string because Hadoop Path is not serializable
    val rasterFiles: Array[String] =
      if (rasterFileSystem.isDirectory(rasterPath)) {
        rasterFileSystem.listStatus(rasterPath, new PathFilter() {
          override def accept(path: Path): Boolean =
            path.getName.toLowerCase().endsWith(".tif") || path.getName.toLowerCase().endsWith(".hdf")
        }).map(p => p.getPath.toString)
      } else {
        Array(rasterPath.toString)
      }
    val rasterReader = RasterHelper.createRasterReader(rasterFileSystem, new Path(rasterFiles(0)), opts, conf)
    try {
      rasterReader.metadata.srid
    } finally {
      rasterReader.close()
    }
  }

  override def addDependentClasses(opts: BeastOptions, classes: util.Stack[Class[_]]): Unit = {
    super.addDependentClasses(opts, classes)
    classes.add(classOf[GeoTiffReader])
  }

  @throws(classOf[IOException])
  override def run(opts: BeastOptions, inputs: Array[String], outputs: Array[String], sc: SparkContext): Unit = {
    sc.hadoopConfiguration.setInt("mapreduce.input.fileinputformat.split.maxsize", 8388608)
    val iLayer = opts.getString(RasterLayer, "0")
    opts.set(IRasterReader.RasterLayerID, iLayer)
    val rasterFile = inputs(1)
    val aggregateFunction = Symbol(opts.getString(AggregateFunction, "aggregates"))
    var vectors: RDD[IFeature] = sc.spatialFile(inputs(0), opts.retainIndex(0))
    if (vectors.getNumPartitions < sc.defaultParallelism) {
      logInfo(s"Repartitioning vector data to ${sc.defaultParallelism} partitions")
      vectors = vectors.repartition(sc.defaultParallelism)
    }
    val collectorClass: Class[_ <: Collector] = aggregateFunction match {
      case 'aggregates => classOf[Statistics]
      case other => throw new RuntimeException(s"Unrecognized aggregate function $other")
    }

    val method = opts.getString(RJMethod, "file")

    // For profiling, count number of processed tiles
    val numTiles = sc.longAccumulator("NumTiles")
    val results: RDD[(IFeature, Collector)] = method.toLowerCase() match {
      case "file" =>  zonalStats(vectors, rasterFile, iLayer, collectorClass, opts, numTiles)
      case "rdd" => zonalStats2(vectors, new RasterFileRDD(sc, rasterFile, opts), collectorClass, opts, numTiles)
    }

    // Write results to the output
    val element = results.first()
    val numBands = element._2.getNumBands
    var schemaElements: Seq[StructField] = element._1.schema.filterNot(_.dataType == GeometryDataType)
    if (numBands == 1) {
      schemaElements = schemaElements ++ Seq(StructField("sum", DoubleType), StructField("count", DoubleType),
        StructField("min", DoubleType), StructField("max", DoubleType))
    } else {
      for (b <- 1 to numBands) {
        schemaElements = schemaElements ++ Seq(StructField(s"sum_$b", DoubleType), StructField("count_$b", DoubleType),
          StructField("min_$b", DoubleType), StructField("max_$b", DoubleType))
      }
    }
    val normalizedResults: SpatialRDD = results.filter(fc => fc._2 != null).map(fc => {
      val statistics: Statistics = fc._2.asInstanceOf[Statistics]
      val statValues: Array[Any] = new Array[Any](numBands * 4)
      val statFields: Array[StructField] = new Array[StructField](numBands * 4)
      for (b <- 0 until numBands) {
        val suffix: String = if (numBands == 1) "" else s"_${b}"
        statValues(4 * b + 0) = statistics.sum(b)
        statFields(4 * b + 0) = StructField("sum"+suffix, DoubleType)
        statValues(4 * b + 1) = statistics.count(b)
        statFields(4 * b + 1) = StructField("count"+suffix, DoubleType)
        statValues(4 * b + 2) = statistics.min(b)
        statFields(4 * b + 2) = StructField("min"+suffix, DoubleType)
        statValues(4 * b + 3) = statistics.max(b)
        statFields(4 * b + 3) = StructField("max"+suffix, DoubleType)
      }
      Feature.concat(fc._1, new GenericRowWithSchema(statValues, StructType(statFields)))
    })
    val oFormat = opts.getString(SpatialWriter.OutputFormat, opts.getString(SpatialFileRDD.InputFormat))
    if (opts.get(CSVFeatureWriter.WriteHeader) == null)
      opts.set(CSVFeatureWriter.WriteHeader, true)
    val bo: BeastOptions = opts
    SpatialWriter.saveFeatures(normalizedResults, oFormat, outputs(0), bo)
    logInfo(s"Number of processed tiles in zonal statistics is ${numTiles.value}")
  }

  /**
    * Computes zonal statistics between a set of zones (polygons) and a raster file given by its path and a layer in
    * that file. The result is an RDD of pairs of a feature and a collector value
    * @param zones a set of polygons that represent the regions or zones
    * @param rasterInput the path that contains the raster files
    * @param iLayer the name of the layer to read from the raster files
    * @param collectorClass the class that collects the pixel values to compute the statistics
    * @param opts additional user-defined options
    * @param numTiles an optional accumulator to collect the total number of processed tiles
    * @return a set of (Feature, Statistics)
    */
  def zonalStats(zones: RDD[IFeature], rasterInput: String, iLayer: String, collectorClass: Class[_ <: Collector],
                 opts: BeastOptions, numTiles: LongAccumulator = null): RDD[(IFeature, Collector)] = {
    val rasterPath: Path = new Path(rasterInput)
    val conf = zones.sparkContext.getConf
    val rasterSRID = getRasterSRID(rasterPath, opts, conf)
    val rasterFileSystem = rasterPath.getFileSystem(opts.loadIntoHadoopConf(null))
    // Store raster files as a list of string because Hadoop Path is not serializable
    val rasterFiles: Array[String] =
      if (rasterFileSystem.isDirectory(rasterPath)) {
        rasterFileSystem.listStatus(new Path(rasterInput), new PathFilter() {
          override def accept(path: Path): Boolean =
            path.getName.toLowerCase().endsWith(".tif") || path.getName.toLowerCase().endsWith(".hdf")
        }).map(p => p.getPath.toString)
      } else {
        Array(rasterPath.toString)
      }

    // Generate a unique ID for each feature
    val idFeatures: RDD[(Long, IFeature)] = zones
      .reproject(CRSServer.sridToCRS(rasterSRID, conf))
      .zipWithUniqueId().map(x => (x._2, x._1))

    // (Feature ID, (x, y, value))
    val featureValue: RDD[(Long, (Int, Int, Float))] = RaptorJoin.raptorJoinFile[Float](rasterFiles, idFeatures, opts, numTiles)
      .map(result => (result.featureID, (result.x, result.y, result.m)))

    // Compute statistics for each feature
    val zeroValue = collectorClass.newInstance()
    zeroValue.setNumBands(1)
    val featureStats: RDD[(Long, Collector)] = featureValue.aggregateByKey(zeroValue)(
      (u: Collector, v: (Int, Int, Float)) => u.collect(v._1, v._2, Array[Float](v._3)),
      (u1: Collector, u2: Collector) => u1.accumulate(u2)
    )

    // Join back with the original features to put back the entire feature and remove the ID
    idFeatures.join(featureStats).map( kv => kv._2)
  }


  /**
   * Computes zonal statistics between a set of zones (polygons) and a raster file given by its path and a layer in
   * that file. The result is an RDD of pairs of a feature and a collector value
   * @param zones a set of polygons that represent the regions or zones
   * @param raster the RDD of tiles
   * @param collectorClass the class that collects the pixel values to compute the statistics
   * @param opts additional user-defined options
   * @param numTiles an optional accumulator to collect the total number of processed tiles
   * @return a set of (Feature, Statistics)
   */
  def zonalStats2(zones: RDD[IFeature], raster: RDD[ITile], collectorClass: Class[_ <: Collector],
                  opts: BeastOptions, numTiles: LongAccumulator = null): RDD[(IFeature, Collector)] = {
    val conf = zones.sparkContext.getConf
    val rasterSRID = raster.first().rasterMetadata.srid
    // Generate a unique ID for each feature
    val idFeatures: RDD[(Long, IFeature)] = zones
      .reproject(CRSServer.sridToCRS(rasterSRID, conf))
      .zipWithUniqueId().map(x => (x._2, x._1))

    // (Feature ID, (x, y, value))
    val featureValue: RDD[(Long, (Int, Int, Float))] = RaptorJoin.raptorJoinIDFull[Float](raster, idFeatures, opts, numTiles)
      .map(result => (result.featureID, (result.x, result.y, result.m)))

    // Compute statistics for each feature
    val zeroValue = collectorClass.newInstance()
    zeroValue.setNumBands(1)
    val featureStats: RDD[(Long, Collector)] = featureValue.aggregateByKey(zeroValue)(
      (u: Collector, v: (Int, Int, Float)) => u.collect(v._1, v._2, Array[Float](v._3)),
      (u1: Collector, u2: Collector) => u1.accumulate(u2)
    )

    // Join back with the original features to put back the entire feature and remove the ID
    idFeatures.join(featureStats).map( kv => kv._2)
  }

  def printUsage(out: PrintWriter): Unit = {
    out.println("zs <vector file> <raster file> layer:<layer> collector:<collector>")
  }
}
