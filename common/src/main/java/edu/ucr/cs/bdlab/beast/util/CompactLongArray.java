package edu.ucr.cs.bdlab.beast.util;

import java.io.Serializable;
import java.util.Iterator;

/**
 * A constant array of long integers stored in a compact and compressed way.
 */
public class CompactLongArray implements Iterable<Long>, Serializable {

  /**The data of all longs*/
  protected long[] data;

  /**Number of elements*/
  protected int numEntries;

  /**Encoding type used to encode the values*/
  enum Encoding {Compact, Delta}

  /**The internal encoding of the values in the bit array*/
  protected Encoding encoding;

  /**
   * If Compact encoding, this is the number of bits to store each value.
   * If Delta encoding, this is the number of bits for the delta part.
   */
  protected int numBits;

  /**
   * If Compact encoding, the smallest of all values.
   * If Delta encoding, the smallest delta.
   */
  protected long base;

  /**
   * Constructs an empty array. Used internally to create sublists.
   */
  protected CompactLongArray() {}

  public CompactLongArray(long[] values) {
    numEntries = values.length;

    // Encode values using delta encoding
    // Calculate the total size needed to encode all values
    long minDelta = Long.MAX_VALUE;
    long maxDelta = Long.MIN_VALUE;
    long minValue = values[0];
    long maxValue = values[0];
    for (int i = 1; i < values.length; i++) {
      minValue = Math.min(minValue, values[i]);
      maxValue = Math.max(maxValue, values[i]);
      long delta = values[i] - values[i - 1];
      minDelta = Math.min(minDelta, delta);
      maxDelta = Math.max(maxDelta, delta);
    }
    long numBitsCompact = numEntries * (long) (MathUtil.log2(maxValue - minValue) + 1);
    long numBitsDelta = 64 + (long) (numEntries - 1) * (MathUtil.log2(maxDelta - minDelta) + 1);
    encoding = numBitsDelta < numBitsCompact? Encoding.Delta : Encoding.Compact;

    long bitPosition;
    switch (encoding) {
      case Compact:
        base = minValue;
        numBits = MathUtil.log2(maxValue - minValue) + 1;
        data = new long[(int)((numBitsCompact + 63) / 64)];
        if (numBits > 0) {
          bitPosition = 0;
          for (int i = 0; i < values.length; i++) {
            MathUtil.setBits(data, bitPosition, numBits, values[i] - base);
            bitPosition += numBits;
          }
          assert bitPosition == numBitsCompact;
        }
        break;
      case Delta:
        base = minDelta;
        numBits = MathUtil.log2(maxDelta - minDelta) + 1;
        assert numBitsDelta <= (64L * Integer.MAX_VALUE) : "Total number of bits is too long";
        data = new long[(int)((numBitsDelta + 63) / 64)];
        // Write entries
        data[0] = values[0];
        if (numBits > 0) {
          bitPosition = 64;
          for (int i = 1; i < values.length; i++) {
            long delta = values[i] - values[i - 1];
            delta -= minDelta;
            MathUtil.setBits(data, bitPosition, numBits, delta);
            bitPosition += numBits;
          }
          assert bitPosition == numBitsDelta;
        }
        break;
    }
  }

  public CompactLongArray(int[] values) {
    numEntries = values.length;

    // Encode values using delta encoding
    // Calculate the total size needed to encode all values
    long minDelta = Long.MAX_VALUE;
    long maxDelta = Long.MIN_VALUE;
    long minValue = values[0];
    long maxValue = values[0];
    for (int i = 1; i < values.length; i++) {
      minValue = Math.min(minValue, values[i]);
      maxValue = Math.max(maxValue, values[i]);
      long delta = values[i] - values[i - 1];
      minDelta = Math.min(minDelta, delta);
      maxDelta = Math.max(maxDelta, delta);
    }
    long numBitsCompact = numEntries * (long) (MathUtil.log2(maxValue - minValue) + 1);
    long numBitsDelta = 64 + (long) (numEntries - 1) * (MathUtil.log2(maxDelta - minDelta) + 1);
    encoding = numBitsDelta < numBitsCompact? Encoding.Delta : Encoding.Compact;

    long bitPosition;
    switch (encoding) {
      case Compact:
        base = minValue;
        numBits = MathUtil.log2(maxValue - minValue) + 1;
        data = new long[(int)((numBitsCompact + 63) / 64)];
        if (numBits > 0) {
          bitPosition = 0;
          for (int i = 0; i < values.length; i++) {
            MathUtil.setBits(data, bitPosition, numBits, values[i] - base);
            bitPosition += numBits;
          }
          assert bitPosition == numBitsCompact;
        }
        break;
      case Delta:
        base = minDelta;
        numBits = MathUtil.log2(maxDelta - minDelta) + 1;
        assert numBitsDelta <= (64L * Integer.MAX_VALUE) : "Total number of bits is too long";
        data = new long[(int)((numBitsDelta + 63) / 64)];
        // Write entries
        data[0] = values[0];
        if (numBits > 0) {
          bitPosition = 64;
          for (int i = 1; i < values.length; i++) {
            long delta = values[i] - values[i - 1];
            delta -= minDelta;
            MathUtil.setBits(data, bitPosition, numBits, delta);
            bitPosition += numBits;
          }
          assert bitPosition == numBitsDelta;
        }
        break;
    }
  }

  /**
   * The total number of elements stored in this LongArray
   * @return the total size in terms of number of elements
   */
  public int size() {
    return numEntries;
  }

  /**
   * Whether this list is empty or not
   * @return {@code true} if the list has no elements
   */
  public boolean isEmpty() {
    return size() == 0;
  }

  @Override
  public String toString() {
    StringBuffer str = new StringBuffer();
    str.append('[');
    boolean first = true;
    for (long l : this) {
      if (first)
        first = false;
      else
        str.append(',');
      str.append(l);
    }
    str.append(']');
    return str.toString();
  }

  /**
   * Creates and returns a CompactLongArray that repeats the given value for the given number of elements.
   * @param value the value to repeat
   * @param size the number of repetitions
   * @return an array of the given size and contains the given value at all positions
   */
  public static CompactLongArray fill(long value, int size) {
    CompactLongArray array = new CompactLongArray();
    array.base = value;
    array.data = null;
    array.numBits = 0;
    array.encoding = Encoding.Compact;
    array.numEntries = size;
    return array;
  }

  public interface CLLIterator extends Iterator<Long> {
    /***
     * Return the next n values as another CompactLongArray. If less than n entries are remaining, all remaining
     * values will be consumed and returned. Callers can use {@link CompactLongArray#size()} to verify.
     * @param n the number of values to return
     * @return a new CompactLongArray with the remaining values. If {@code n=0}, {@code null} is returned.
     */
    CompactLongArray next(int n);
  }

  @Override
  public CLLIterator iterator() {
    switch (encoding) {
      case Delta: return new LongIteratorDelta();
      case Compact: return new LongIteratorCompact();
      default: throw new RuntimeException("Unsupported encoding type "+encoding);
    }
  }

  /**
   * An iterator when the data is encoded using Delta encoding
   */
  class LongIteratorDelta implements CLLIterator {

    /**The index of the next value to return*/
    int i = 0;

    /**The last value returned*/
    long lastValueReturned;

    /**The bit position to read from*/
    long bitPosition = 64;

    @Override
    public boolean hasNext() {
      return i < numEntries;
    }

    @Override
    public Long next() {
      assert i < numEntries;
      i++;
      if (i == 1)
        return lastValueReturned = data[0];
      long delta = (numBits == 0? 0 : MathUtil.getBits(data, bitPosition, numBits)) + base;
      bitPosition += numBits;
      lastValueReturned += delta;
      return lastValueReturned;
    }

    @Override
    public CompactLongArray next(int n) {
      n = Math.min(n, size() - i);
      if (n == 0)
        return null;
      CompactLongArray subarray = new CompactLongArray();
      subarray.numEntries = n;
      subarray.encoding = encoding;
      subarray.numBits = numBits;
      subarray.base = base;
      int numBitsDeltas = (n - 1) * numBits;
      subarray.data = new long[1 + (numBitsDeltas + 63) / 64];
      subarray.data[0] = this.next();
      if (numBits > 0) {
        int iEntry = 1;
        long copyBitPosition = bitPosition;
        while (numBitsDeltas >= 64) {
          subarray.data[iEntry++] = MathUtil.getBits(data, copyBitPosition, 64);
          copyBitPosition += 64;
          numBitsDeltas -= 64;
        }
        // Last entry
        if (numBitsDeltas > 0)
          subarray.data[iEntry++] = MathUtil.getBits(data, copyBitPosition, numBitsDeltas) << (64 - numBitsDeltas);
        assert iEntry == subarray.data.length;
        // Update the iterator to make it ready for the next step.
        // We have to call the next function to ensure that lastValueReturned is updated along with bitPosition and i.
        for (int j = 0; j < n - 1; j++)
          this.next();
      } else {
        // Update the iterator to reflect the steps we made
        // This special case can be done more efficiently since all deltas are equal and numBits is zero
        i += n - 1;
        lastValueReturned += (n - 1) * base;
      }
      return subarray;
    }
  }

  class LongIteratorCompact implements CLLIterator {
    /**The index of the next value to return*/
    int i = 0;

    /**The bit position to read from*/
    long bitPosition = 0;

    @Override
    public boolean hasNext() {
      return i < numEntries;
    }

    @Override
    public Long next() {
      assert i < numEntries;
      i++;
      long value = (numBits == 0 ? 0 : MathUtil.getBits(data, bitPosition, numBits)) + base;
      bitPosition += numBits;
      return value;
    }

    @Override
    public CompactLongArray next(int n) {
      n = Math.min(n, size() - i);
      if (n == 0)
        return null;
      CompactLongArray subarray = new CompactLongArray();
      subarray.numEntries = n;
      subarray.encoding = encoding;
      subarray.numBits = numBits;
      subarray.base = base;
      if (numBits > 0) {
        int numBitsToCopy = n * numBits;
        subarray.data = new long[(numBitsToCopy + 63) / 64];
        int iEntry = 0;
        while (numBitsToCopy >= 64) {
          subarray.data[iEntry++] = MathUtil.getBits(data, bitPosition, 64);
          bitPosition += 64;
          numBitsToCopy -= 64;
        }
        // Last entry
        if (numBitsToCopy > 0) {
          subarray.data[iEntry++] = MathUtil.getBits(data, bitPosition, numBitsToCopy) << (64 - numBitsToCopy);
          bitPosition += numBitsToCopy;
        }
        assert iEntry == subarray.data.length;
        i += n;
      }
      return subarray;
    }
  }

}
